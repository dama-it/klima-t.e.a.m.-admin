
## 1.4.3

- S3 Image Upload/Download changes
- refator search behaviour

## 1.3.3

- fix user comments

## 1.3.2

- Refactor search behaviour and projections

## 1.3.1

- fix post update with comments

## 1.3.0

- push messages

## 1.2.0

- adding challenges
- adding push notifications
- bug fixes

## 0.3.0
- adding post comments 

## 0.2.0
- refactoring of model structure
- refactoring of controller structure
- refactoring of exception-handling
- refactoring of authentication/authorization
- added Posts (add, edit, list)
- added moderator actions on posts (hide, delete, pin)
- added app-user actions on posts (vote up, vote down, fav)


## 0.1.0
Initial Version

### Added
- Added admin user management
- Added user role management with permissions
- Added app u ser management
- Added topic management
- Added moderator management
- First theme implemented