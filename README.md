## Installation (development)

### Database

```shell
docker-compose up -d
```

### Backend

#### Application Properties

```shell
cp src/main/resources/application.properties.dist src/main/resources/application.properties
```

#### Install and Run Maven

```shell
mvn clean install && 
mvn spring-boot:run
```
 
### Frontend

#### Install and Run Yarn

```shell
cd src/main/app && 
yarn clean install &&
yarn start
```


