package dama.lailaa.authApp.services;

import static java.util.Collections.emptyList;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import dama.lailaa.authApp.services.AppUserService;
import dama.lailaa.app.models.AppUser;

@Service("appUserDetailsService")
public class AppUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private AppUserService appUserService;

    @Override
    public AppUserRepositoryUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user = appUserService.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        AppUserRepositoryUserDetails userRepositoryUserDetails = new AppUserRepositoryUserDetails(user.getUsername(),user.getPassword(), emptyList());
        userRepositoryUserDetails.setActive(user.isActive());
        return userRepositoryUserDetails;
    }
    private final static class AppUserRepositoryUserDetails extends User implements UserDetails {
        private static final long serialVersionUID = 1L;
        private boolean active;

        public AppUserRepositoryUserDetails(String username, String password,
                Collection<? extends GrantedAuthority> authorities) {
            super(username, password, authorities);
        }

        // another methods
        public void setActive(boolean active) {
            this.active = active;
        }

        @Override
        public boolean isEnabled() {
            return this.active;
        }

        @Override
        public String toString() {
            return this.getUsername();
        }
        
    }

}