package dama.lailaa.authApp.services;

import dama.lailaa.authApp.services.AppUserService;
import dama.lailaa.app.models.AppUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class CurrentAppUserServiceImpl implements CurrentAppUserService {

    @Autowired
    private AppUserService appUserService;

    @Override
    public boolean hasPermission(Principal user, String permission) {

        if (user == null) {
            return false;
        }

        // AppUser currentUser =
        // appUserRepository.findOneByUsernameAndEnabledIsTrue(user.getName());
        // boolean hasPermission = false;

        // TODO: do app-users have permissions??
        return true;
    }

    @Override
    public boolean hasOneOfPermissions(Principal user, String[] permissions) {

        if (user == null) {
            return false;
        }

        // AppUser currentUser =
        // appUserRepository.findOneByUsernameAndEnabledIsTrue(user.getName());

        // boolean hasPermission = false;

        // TODO: are there any permissions for appuser??

        return true;
    }

    @Override
    public boolean hasPermissionOrId(Principal user, String permission, long id) {
        boolean hasPermission = this.hasPermission(user, permission);
        boolean isSameUser = this.canAccessUser(user, id);

        return hasPermission || isSameUser;
    }

    @Override
    public boolean canAccessUser(Principal user, long id) {
        if (user == null)
            return false;
        AppUser currentUser = appUserService.findByUsername(user.getName());
        return currentUser != null && currentUser.getId() == id;
    }
}
