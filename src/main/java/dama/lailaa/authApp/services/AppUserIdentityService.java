package dama.lailaa.authApp.services;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.stereotype.Service;

import dama.lailaa.authApp.services.AppUserService;
import dama.lailaa.authApp.models.IdentityDto;
import dama.lailaa.authApp.models.IdentityProjection;
import dama.lailaa.app.models.AppUser;

@Service
public class AppUserIdentityService {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private ProjectionFactory projectionFactory;

    public IdentityDto getIdentity(Principal principal) throws Exception {
        try {
            String username = principal.getName();
            AppUser currentUser = appUserService.findByUsername(username);
            IdentityDto identityContainer = new IdentityDto();
            IdentityProjection projection = projectionFactory.createProjection(IdentityProjection.class, currentUser);
            identityContainer.setIdentity(projection);
            return identityContainer;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("ERROR.ERROR");
        }
    }

}
