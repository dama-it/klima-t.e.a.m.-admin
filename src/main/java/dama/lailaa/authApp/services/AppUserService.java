package dama.lailaa.authApp.services;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import dama.lailaa.app.models.AppUser;
import dama.lailaa.app.repositories.AppUserRepository;

@Service
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    public AppUser findByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    public AppUser findById(long id){
        return appUserRepository.findById(id).orElseThrow(null);
    }

    public AppUser findByIdOrNull(long id) {
        return appUserRepository.findById(id).orElse(null);
    }
    public AppUser save(AppUser appUser) {
        return appUserRepository.save(appUser);
    }   
}