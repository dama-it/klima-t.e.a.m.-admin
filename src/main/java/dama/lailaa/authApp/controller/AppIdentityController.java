package dama.lailaa.authApp.controller;

import dama.lailaa.authApp.models.IdentityDto;
import dama.lailaa.authApp.models.IdentityProjection;
import dama.lailaa.authApp.services.AppUserIdentityService;
import dama.lailaa.app.models.AppUser;
import dama.lailaa.app.repositories.AppUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("app/identity")
public class AppIdentityController {

    @Autowired
    AppUserIdentityService appUserIdentityService;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private ProjectionFactory projectionFactory;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public IdentityDto getIdentity(Principal principal) throws Exception {
        String username = principal.getName();
        AppUser currentUser = appUserRepository.findByUsername(username);
        
        IdentityDto identityContainer = new IdentityDto();
        IdentityProjection projection = projectionFactory.createProjection(IdentityProjection.class, currentUser);
        identityContainer.setIdentity(projection);
        return identityContainer;
    }

}