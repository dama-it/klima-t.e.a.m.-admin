// package dama.lailaa.authApp.controller;

// import dama.lailaa.authApp.models.IdentityDto;
// import dama.lailaa.authApp.models.IdentityProjection;
// import dama.lailaa.authApp.models.RegMembershipDto;
// import dama.lailaa.authApp.models.RegistrationDto;
// import dama.lailaa.authApp.services.AppUserIdentityService;
// import dama.lailaa.mail.EmailService;
// import dama.lailaa.app.models.AppUser;
// import dama.lailaa.app.repositories.AppUserRepository;
// import dama.lailaa.application.exceptions.ViewException;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.data.projection.ProjectionFactory;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.ResponseBody;
// import org.springframework.web.bind.annotation.RestController;

// import java.security.Principal;
// import java.security.SecureRandom;
// import java.util.ArrayList;
// import java.util.Date;
// import java.util.List;
// import java.util.Optional;

// @RestController
// @RequestMapping("app/registration")
// public class RegistrationController {

//     @Autowired
//     private AppUserRepository appUserRepository;

//     @Autowired
//     EmailService emailService;

//     @RequestMapping(value = "reg", method = RequestMethod.POST)
//     @ResponseBody
//     public void register(RegistrationDto registrationDto, Principal principal) throws Exception {
        

//         AppUser user = appUserRepository.findByUsername(registrationDto.getUsername());
        
//         if (user != null) {
//             throw new ViewException("USERNAME_ALREADY_EXIST");
//         }
//         else{
//             AppUser a = new AppUser();
//             // a.setHeight(registrationDto.getHeight());
//             // a.setWeight(registrationDto.getWeight());
//             // //a.setBirthDate(registrationDto.getDate());
//             // //a.setGender(registrationDto.getGender());
//             // a.setCountry(registrationDto.getCountry());
//             // a.setUsername(registrationDto.getUsername());
//             // //a.setPassword(registrationDto.getPassword());
//             // a.setEmail(registrationDto.getEmail());
//             // a.setFirstName(registrationDto.getFirstName());
//             // a.setLastName(registrationDto.getLastName());

//             appUserRepository.save(a);
//         }
//     }

//     @RequestMapping(value = "reg/membership", method = RequestMethod.POST)
//     @ResponseBody
//     public void registerWithMembership(RegMembershipDto registrationDto, Principal principal) throws Exception {
        

//         AppUser user = appUserRepository.findByUsername(registrationDto.getUsername());
        
//         if (user != null) {
//             List<Membership> membershipList = user.getMembershipList();
            
//             for (int i=0; i < registrationDto.getMembership().size(); i++) {
//                 Optional<Membership> currentMembership = membershipRepository.findById(Long.parseLong(registrationDto.getMembership().get(i)));
//                 membershipList.add(currentMembership.get());
//             }

//             user.setMembershipList(membershipList);
//             appUserRepository.save(user);
//         }
//         else{
//             AppUser a = new AppUser();
//             String pass = generateRandomPassword(6);
//             a.setHeight(registrationDto.getHeight());
//             a.setWeight(registrationDto.getWeight());
//             a.setCountry(registrationDto.getCountry());
//             a.setUsername(registrationDto.getUsername());
//             a.setPassword(pass);
//             a.setEmail(registrationDto.getEmail());
//             a.setFirstName(registrationDto.getFirstName());
//             a.setLastName(registrationDto.getLastName());
            
//             if(registrationDto.getMembership().size() != 0){
//                 List<Membership> membershipList = new ArrayList<>();
//                 for (int i=0; i < registrationDto.getMembership().size(); i++) {
//                     Optional<Membership> currentMembership = membershipRepository.findById(Long.parseLong(registrationDto.getMembership().get(i)));
//                     membershipList.add(currentMembership.get());
//                 }

//                 a.setMembershipList(membershipList);
//             }

//             appUserRepository.save(a);

//             System.out.println(pass);

//             emailService.sendSimpleMessage(a.getEmail(), "Registration for Lailaa", "Your username is: "+ a.getUsername() +"\nYour password is: "+ pass);

//         }
//     }

//     public static String generateRandomPassword(int len)
//     {
//         final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
//         SecureRandom random = new SecureRandom();
//         StringBuilder sb = new StringBuilder();
 
//         for (int i = 0; i < len; i++)
//         {
//             int randomIndex = random.nextInt(chars.length());
//             sb.append(chars.charAt(randomIndex));
//         }
 
//         return sb.toString();
//     }
// }