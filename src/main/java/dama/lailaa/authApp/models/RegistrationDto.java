package dama.lailaa.authApp.models;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrationDto {

    protected String title;
    protected String username;
    protected String password;
    protected String email;
    protected String firstName;
    protected String lastName;
    protected float height;
    protected float weight;
    protected Date date;
    protected String country;
    protected String gender;

}