package dama.lailaa.authApp.models;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import dama.lailaa.app.models.AppUser;

@Projection(name = "identityProjection", types = { AppUser.class })
public interface IdentityProjection {
    @Value("#{target.getId()}")
    public long getId();

    @Value("#{target.getUsername()}")
    public String getUsername();

    @Value("#{target.getEmail()}")
    public String getUserEmail();

    @Value("#{target.getFirstName()}")
    public String getUserFirstName();

    @Value("#{target.getLastName()}")
    public String getUserLastName();

    @Value("#{target.getFirstName()} #{target.getLastName()}")
    public String getUserFullName();

    @Value("#{target.getPoints()}")
    public Integer getPoints();

    @Value("#{target.isActive()}")
    public Boolean getActive();

    @Value("#{target.isCalibration()}")
    public Boolean getCalibration();



}