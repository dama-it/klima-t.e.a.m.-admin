package dama.lailaa.authApp.security;

public class SecurityConstants {
    public static final String SECRET = "JKq5axNdqLiMiNWLYeCcdPj6WoTMP8XobaWym7iVWUWaRVvohBCuHiJLguDDv2x";
    public static final long EXPIRATION_TIME = 36000_000; // 10 hours
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/app/register";
}
