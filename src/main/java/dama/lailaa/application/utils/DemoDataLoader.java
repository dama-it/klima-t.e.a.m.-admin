package dama.lailaa.application.utils;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


@Component
@Order(999) // always run last
public class DemoDataLoader implements CommandLineRunner {


    @Override
    public void run(String... strings) throws Exception {

    }
}