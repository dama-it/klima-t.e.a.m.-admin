package dama.lailaa.application.utils;

import dama.lailaa.auth.models.User;
import dama.lailaa.auth.models.UserRole;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.repositories.UserRoleRepository;
import dama.lailaa.auth.security.UserRoles;
import dama.lailaa.auth.security.permissions.InternalPermission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.HashSet;

import java.util.Optional;
import java.util.Set;

@Component
@Order(1)
public class AuthDatabaseLoader implements CommandLineRunner {
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;

    @Autowired
    public AuthDatabaseLoader(UserRepository userRepository, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        // create userroles
        Optional<User> optCheck = userRepository.findById((long)1);
        if(!optCheck.isPresent()) {
            UserRole adminRole = userRoleRepository.findByName(UserRoles.ROLE_ADMIN);
            if(adminRole == null) {
                adminRole = new UserRole(UserRoles.ROLE_ADMIN);
                adminRole.addPermission(InternalPermission.IS_ADMIN);
            }
            
            Set<UserRole> userRoles = new HashSet<>(0);
            userRoles.add(adminRole);

            // create initial admin user
            User admin = new User(
                    "admin",
                    "D40min20",
                    "info@dama.link",
                    "DAMA",
                    "IT d.o.o.",
                    userRoles,
                    null,
                    true
            );
            userRepository.save(admin);
        }
    }
}
