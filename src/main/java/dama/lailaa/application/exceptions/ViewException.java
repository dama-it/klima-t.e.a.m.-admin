package dama.lailaa.application.exceptions;

public class ViewException extends Exception {

    private final String message;

    public ViewException(String msg) {
        super("ERROR." + msg);
        this.message = msg;
    }

    public String getMessage() {
        return message;
    }

    private static final long serialVersionUID = 1L;
}