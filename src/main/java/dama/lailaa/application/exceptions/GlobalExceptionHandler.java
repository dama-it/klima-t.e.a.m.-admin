package dama.lailaa.application.exceptions;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.util.WebUtils;

import dama.lailaa.application.models.ApiError;

@ControllerAdvice
public class GlobalExceptionHandler {
    /** Provides handling for exceptions throughout this service. */
    @ExceptionHandler({ ConstraintViolationException.class, ListViewException.class, FormValidException.class,
            Exception.class })
    public final ResponseEntity<ApiError> handleException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        if (ex instanceof ConstraintViolationException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            ConstraintViolationException cvex = (ConstraintViolationException) ex;
            return handleConstraintViolationException(cvex, headers, status, request);
        } else if (ex instanceof FormValidException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            FormValidException fvex = (FormValidException) ex;
            return handleFormValidException(fvex, headers, status, request);
        } else if (ex instanceof ListViewException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            ListViewException lvex = (ListViewException) ex;
            return handleListViewException(lvex, headers, status, request);
        } else if (ex instanceof MaxUploadSizeExceededException) {
            MaxUploadSizeExceededException mUEx = (MaxUploadSizeExceededException) ex;
            HttpStatus status = HttpStatus.BAD_REQUEST;
            return handleMaxUploadSizeExceededException(mUEx, headers, status, request);
        } else {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            return handleOtherException(ex, headers, status, request);
        }
    }

    protected ResponseEntity<ApiError> handleMaxUploadSizeExceededException(MaxUploadSizeExceededException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = Collections.singletonList("ERROR.MAX_UPLOAD_SIZE_EXCEEDED");
        System.out.println(errors);
        return handleExceptionInternal(ex, new ApiError(errors), headers, status, request);
    }

    protected ResponseEntity<ApiError> handleConstraintViolationException(ConstraintViolationException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = ex.getConstraintViolations().stream()
                .map(constrainViolation -> "ERROR." + constrainViolation.getMessage()).collect(Collectors.toList());
        System.out.println(errors);
        return handleExceptionInternal(ex, new ApiError(errors), headers, status, request);
    }

    protected ResponseEntity<ApiError> handleListViewException(ListViewException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        List<String> errorMessages = Collections.singletonList(ex.getMessage());
        System.out.println(errorMessages);
        return handleExceptionInternal(ex, new ApiError(errorMessages), headers, status, request);
    }

    protected ResponseEntity<ApiError> handleOtherException(Exception ex, HttpHeaders headers, HttpStatus status,
            WebRequest request) {
        List<String> errorMessages = Collections.singletonList("ERROR.ERROR");
        System.out.println(ex.getMessage());
        ex.printStackTrace();
        return handleExceptionInternal(ex, new ApiError(errorMessages), headers, status, request);
    }

    protected ResponseEntity<ApiError> handleFormValidException(FormValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        List<String> errorMessages = Collections.singletonList(ex.getMessage());
        System.out.println(errorMessages);
        return handleExceptionInternal(ex, new ApiError(errorMessages), headers, status, request);
    }

    /** A single place to customize the response body of all Exception types. */
    protected ResponseEntity<ApiError> handleExceptionInternal(Exception ex, ApiError body, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }

        return new ResponseEntity<>(body, headers, status);
    }
}