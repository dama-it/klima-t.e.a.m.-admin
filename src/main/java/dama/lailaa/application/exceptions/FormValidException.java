package dama.lailaa.application.exceptions;

public class FormValidException extends Exception {

    public String message;

    public FormValidException(String msg) {
        super("ERROR." + msg);
        this.message = msg;
    }

    @Override
    public String toString() {
        return this.getMessage();
    }

    private static final long serialVersionUID = 1L;

}