package dama.lailaa.application.exceptions;

public class ListViewException extends Exception {

    private final String message;

    public ListViewException(String msg) {
        super("ERROR." + msg);
        this.message = msg;
    }

    public String getMessage() {
        return message;
    }

    private static final long serialVersionUID = 1L;
}
