package dama.lailaa.application.config;

import dama.lailaa.authApp.security.JWTAuthenticationFilter;
import dama.lailaa.authApp.security.JWTAuthorizationFilter;
import dama.lailaa.authApp.security.SecurityConstants;
import dama.lailaa.authApp.services.AppUserDetailsServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.ImmutableList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
@Order(2)
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
public class WebSecurityConfigApp extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsServiceApp() {
        return new AppUserDetailsServiceImpl();
    };

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/app/**").cors().and().csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST, SecurityConstants.SIGN_UP_URL).permitAll()
                .antMatchers(HttpMethod.POST, "/app/passwordRequest").permitAll()
                .antMatchers(HttpMethod.POST, "/app/passwordRequestCode").permitAll()
                .antMatchers(HttpMethod.POST, "/app/passwordReset/*").permitAll()
                .antMatchers(HttpMethod.GET, "/app/passwordReset/*").permitAll()
                .antMatchers(HttpMethod.POST, "/app/registration/**").permitAll()
                .antMatchers(HttpMethod.GET, "/app/privacy/*").permitAll()
                .antMatchers(HttpMethod.GET, "/app/ical/*").permitAll()
                .antMatchers(HttpMethod.GET, "/app/locales/**").permitAll().antMatchers(HttpMethod.GET, "/app/config/")
                .permitAll().antMatchers(HttpMethod.GET, "/app/download/*").permitAll()
                .antMatchers(HttpMethod.POST, "/passwordReset").permitAll().antMatchers(HttpMethod.POST, "/app/login")
                .permitAll().antMatchers(HttpMethod.GET, "/app/verify/**").permitAll().anyRequest().authenticated()
                .and().formLogin().loginPage("/app/login").successHandler(new AuthentificationLoginSuccessHandler())
                .permitAll(true).failureHandler(new SimpleUrlAuthenticationFailureHandler()).and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().exceptionHandling()
                .and().exceptionHandling().accessDeniedHandler((request, response, accessDeniedException) -> {
                    response.sendError(HttpServletResponse.SC_FORBIDDEN, "ERROR.ACCESS_DENIED_FORBIDDEN");
                }).authenticationEntryPoint((request, response, authException) -> {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "ERROR.ACCESS_DENIED_UNAUTHORIZED");
                });

    }

    private class AuthentificationLoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
        @Override
        public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                org.springframework.security.core.Authentication authentication) throws IOException, ServletException {

            String token = Jwts.builder().setSubject((String) authentication.getPrincipal().toString())
                    .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                    .signWith(SignatureAlgorithm.HS512, SecurityConstants.SECRET.getBytes()).compact();
            response.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().write(
                    "{\"" + SecurityConstants.HEADER_STRING + "\":\"" + SecurityConstants.TOKEN_PREFIX+token + "\"}"
            );
            response.setStatus(HttpServletResponse.SC_OK);
        }
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceApp()).passwordEncoder(passwordEncoder());
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(ImmutableList.of("*"));
        configuration
                .setAllowedMethods(ImmutableList.of("HEAD", "GET", "POST", "PUT", "DELETE", "DELETE_MANY", "PATCH"));
        // setAllowCredentials(true) is important, otherwise:
        // The value of the 'Access-Control-Allow-Origin' header in the response must
        // not be the wildcard '*' when the request's credentials mode is 'include'.
        configuration.setAllowCredentials(true);
        // setAllowedHeaders is important! Without it, OPTIONS preflight request
        // will fail with 403 Invalid CORS request
        configuration.setAllowedHeaders(ImmutableList.of("Authorization", "Cache-Control", "Content-Type"));
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}