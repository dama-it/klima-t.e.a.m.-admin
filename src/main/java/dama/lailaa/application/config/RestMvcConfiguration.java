package dama.lailaa.application.config;

import dama.lailaa.auth.models.User;
import dama.lailaa.auth.models.UserRole;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class RestMvcConfiguration implements WebMvcConfigurer {
    
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(User.class, UserRole.class); // -> add Entity classes ,<Class>,...
    }
}
