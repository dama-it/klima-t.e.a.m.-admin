package dama.lailaa.backend.models;


public class RawFile{

    private long documentId;

    public long getDocumentId() {
        return documentId;
    }

    public void setDocumentId(long documentId) {
        this.documentId = documentId;
    }
}
