package dama.lailaa.backend.models;


public class File{
    private RawFile rawFile;

    public RawFile getRawFile() {
        return rawFile;
    }

    public void setRawFile(RawFile rawFile) {
        this.rawFile = rawFile;
    }

}
