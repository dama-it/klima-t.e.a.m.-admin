package dama.lailaa.backend.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectDto {

    protected String name;
    protected String author;
    protected String customerName;
    protected String customerAddress;
    protected String customerPhone;
    protected String customerEmail;
    private Document image;

}