package dama.lailaa.backend.models;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "DocumentMinProjection", types = { Document.class })
public interface DocumentMinProjection {
    long getId();

    String getPath();

    String getDescription();
}