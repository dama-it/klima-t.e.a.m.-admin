package dama.lailaa.backend.controller;

import java.security.Principal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import dama.lailaa.application.exceptions.FormValidException;
import dama.lailaa.application.exceptions.ViewException;
import dama.lailaa.auth.models.User;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.security.permissions.InternalPermission;
import dama.lailaa.backend.models.Panel;
import dama.lailaa.backend.models.Project;
import dama.lailaa.backend.repositories.PanelRepository;

@RestController
@RequestMapping("api/panels")
// doesn't work with PreAuthorize?
// @BasePathAwareController
public class PanelController {

    @Autowired
    PanelRepository panelRepository;

    @Autowired
    UserRepository userRepository;
 
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Page<Panel> findAll(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters,
            Principal principal) throws Exception {
        return panelRepository.findAll(pageable);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Panel findOne(@PathVariable long id, Principal principal) throws Exception {
        Optional<Panel> existingPanel = panelRepository.findById(id);

        if (existingPanel.isPresent() == false) {
            throw new ViewException("CLIENT_DOES_NOT_EXIST");
        }

        User currentUser = userRepository.findByUsername(principal.getName());

        if (!currentUser.hasPermission(InternalPermission.IS_ADMIN)
                && currentUser.getClient().getId() != existingPanel.get().getId()) {
            throw new ViewException("PERMISSION_DENIED");
        }
        return existingPanel.get();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Panel create(@RequestBody Panel panel, Principal principal) throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());
        Panel panelNew = new Panel();
        panelNew.setHeight(panel.getHeight());
        panelNew.setLength(panel.getLength());
        panelNew.setWidth(panel.getWidth());
        panelNew.setPower(panel.getPower());
        panelNew.setName(panel.getName());
        panelNew.setUser(currentUser);
        panelRepository.save(panelNew);
        return panelNew;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + InternalPermission.IS_ADMIN + "')")
    @ResponseBody
    public Panel update(@PathVariable long id, @RequestBody Panel panel, Principal principal) throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());
        Optional<Panel> panelExistingOpt = panelRepository.findById(id);
        Panel panelExisting = panelExistingOpt.get();
        panelExisting.setHeight(panel.getHeight());
        panelExisting.setLength(panel.getLength());
        panelExisting.setWidth(panel.getWidth());
        panelExisting.setPower(panel.getPower());
        panelExisting.setName(panel.getName());
        panelExisting.setUser(currentUser);
        panelRepository.save(panelExisting);
        return panelExisting;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + InternalPermission.IS_ADMIN + "')")
    @ResponseBody
    public Panel delete(@PathVariable long id, Principal principal) throws Exception {
        Optional<Panel> existingPanel = panelRepository.findById(id);

        if (existingPanel.isPresent() == false) {
            throw new FormValidException("CLIENT_DOES_NOT_EXIST");
        }
        existingPanel.get().setDeleted(true);
        return existingPanel.get();
    }
}
