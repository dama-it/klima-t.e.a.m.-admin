package dama.lailaa.backend.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import dama.lailaa.auth.models.User;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.security.permissions.ClientPermission;
import dama.lailaa.auth.security.permissions.InternalPermission;
import dama.lailaa.backend.models.Document;
import dama.lailaa.backend.repositories.DocumentRepository;

@Controller
public class UploadController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @Value("${dama.lailaa.fileUploadPath}")
    private String fileUploadPath;

    private List<String> allowedFileExtensions = Arrays.asList("pdf", "png", "jpg", "jpeg");

    @PostMapping(value = "api/upload")
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public Document handleFileUpload(Principal principal, @RequestParam("name") String name,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam("file") MultipartFile file) throws Exception {

        User currentUser = userRepository.findByUsername(principal.getName());

        if (name.contains("/")) {
            throw new Exception("Folder separators not allowed.");
        } else if (name.contains("/")) {
            throw new Exception("Relative pathnames not allowed.");
        }

        boolean validExtension = false;
        for (String extension : allowedFileExtensions) {
            if (name.toLowerCase().endsWith("." + extension)) {
                validExtension = true;
            }
        }
        if (!validExtension) {
            throw new Exception("File type not allowed.");
        }

        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Calendar cal = Calendar.getInstance();
                long currentTimeMilli = cal.getTimeInMillis();
                String fileExtension = getExtensionByStringHandling(name);
                String generatedFileName = String.valueOf(currentTimeMilli) + "." + fileExtension;
                String fullPath = fileUploadPath + generatedFileName;
                String contentType = file.getContentType();
                Document document = new Document();
                document.setFileType(contentType);
                document.setUser(currentUser);
                document.setClient(currentUser.getClient());
                document.setFileName(name);
                document.setPath(generatedFileName);
                document.setDescription(description);
                document.setDeleted(LocalDateTime.now());
                File newFile = new File(fullPath);
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
                stream.write(bytes);
                stream.close();
                documentRepository.save(document);
                return document;
            } catch (Exception e) {
                throw new Exception(e.getMessage());
            }
        } else {
            throw new Exception("file " + name + " was empty.");
        }
    }

    private String getExtensionByStringHandling(String filename) {
        if (filename.contains(".")) {
            return filename.substring(filename.lastIndexOf(".") + 1);
        }
        return null;
    }
}
