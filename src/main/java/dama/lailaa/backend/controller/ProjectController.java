package dama.lailaa.backend.controller;

import java.security.Principal;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import dama.lailaa.application.exceptions.FormValidException;
import dama.lailaa.application.exceptions.ViewException;
import dama.lailaa.auth.models.User;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.security.permissions.InternalPermission;
import dama.lailaa.backend.models.Customer;
import dama.lailaa.backend.models.Document;
import dama.lailaa.backend.models.Project;
import dama.lailaa.backend.models.ProjectDto;
import dama.lailaa.backend.repositories.CustomerRepository;
import dama.lailaa.backend.repositories.DocumentRepository;
import dama.lailaa.backend.repositories.ProjectRepository;

@RestController
@RequestMapping("api/projects")
// doesn't work with PreAuthorize?
// @BasePathAwareController
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    
    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private CustomerRepository customerRepository;
 
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Page<Project> findAll(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters, Principal principal) throws Exception {
        return projectRepository.findAllSearch(pageable,parameters.getFirst("fullText") == null ? "": parameters.getFirst("fullText"));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Project findOne(@PathVariable long id, Principal principal) throws Exception {
        Optional<Project> existingProject = projectRepository.findById(id);

        if (existingProject.isPresent() == false) {
            throw new ViewException("CLIENT_DOES_NOT_EXIST");
        }

        User currentUser = userRepository.findByUsername(principal.getName());

        if (!currentUser.hasPermission(InternalPermission.IS_ADMIN)
                && currentUser.getClient().getId() != existingProject.get().getId()) {
            throw new ViewException("PERMISSION_DENIED");
        }
        return existingProject.get();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Project create(@RequestBody ProjectDto projectDto, Principal principal) throws Exception {
      
        System.out.println(".create");

        Customer c = new Customer();
        c.setAddress(projectDto.getCustomerAddress());
        c.setEmail(projectDto.getCustomerEmail());
        c.setName(projectDto.getCustomerName());
        c.setPhone(projectDto.getCustomerPhone());
        customerRepository.save(c);

        Project p = new Project();
        p.setAuthor(projectDto.getAuthor());
        p.setCustomer(c);
        p.setName(projectDto.getName());
        
        if (projectDto.getImage() != null) {
            Optional<Document> picture = documentRepository.findById(projectDto.getImage().getId());
            if (picture.isPresent()) {
                Document pictureDoc = picture.get();
                p.setImage(pictureDoc);
            }
        }

        projectRepository.save(p);
      
        return p;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + InternalPermission.IS_ADMIN + "')")
    @ResponseBody
    public Project update(@PathVariable long id, @RequestBody ProjectDto projectDto, Principal principal) throws Exception {
       
        System.out.println(".update");

        Optional<Project> po = projectRepository.findById(id);
        Project p = po.get();

        Optional<Customer> co = customerRepository.findById(p.getCustomer().getId());
        Customer c = co.get();

        c.setAddress(projectDto.getCustomerAddress());
        c.setEmail(projectDto.getCustomerEmail());
        c.setName(projectDto.getCustomerName());
        c.setPhone(projectDto.getCustomerPhone());
        p.setAuthor(projectDto.getAuthor());
        p.setCustomer(c);
        p.setName(projectDto.getName());
        if (projectDto.getImage() != null) {
            Optional<Document> picture = documentRepository.findById(projectDto.getImage().getId());
            if (picture.isPresent()) {
                Document pictureDoc = picture.get();
                p.setImage(pictureDoc);
            }
        }
        projectRepository.save(p);
        customerRepository.save(c);

        return null;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + InternalPermission.IS_ADMIN + "')")
    @ResponseBody
    public Project delete(@PathVariable long id, Principal principal) throws Exception {
        Optional<Project> existingProject = projectRepository.findById(id);

        if (existingProject.isPresent() == false) {
            throw new FormValidException("CLIENT_DOES_NOT_EXIST");
        }
        existingProject.get().setDeleted(true);
        return existingProject.get();
    }
}
