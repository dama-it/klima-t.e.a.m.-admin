package dama.lailaa.backend.controller;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import dama.lailaa.backend.models.Document;
import dama.lailaa.backend.repositories.DocumentRepository;

@Controller
public class DownloadController {

    @Value("${dama.lailaa.fileUploadPath}")
    private String fileUploadPath;

    @Autowired
    private DocumentRepository documentRepository;

    @GetMapping("api/download/{id}")
    public ResponseEntity<byte[]> downloadFile(@PathVariable long id, HttpServletRequest request) throws Exception {
        // Load file as Resource
        Resource resource = this.loadFileAsResource(id);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            throw new Exception("COULD_NOT_DETERMINE_FILE_TYPE");

        }

        /*
         * // Fallback to the default content type if type could not be determined if
         * (contentType == null) { contentType = "application/octet-stream"; }
         */

        HttpHeaders header = new HttpHeaders();

        Optional<Document> document = documentRepository.findById(id);
        if (!document.isPresent()) {
            throw new Exception("FILE_NOT_FOUND");
        }

        byte[] bytes = Files.readAllBytes(Paths.get(fileUploadPath + document.get().getPath()));

        header.setContentType(MediaType.valueOf(contentType));

        header.setContentLength(bytes.length);

        header.set("Content-Disposition", "attachment; filename=" + document.get().getFileName());
        return new ResponseEntity<>(bytes, header, HttpStatus.OK);

        /*
         * return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
         * .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" +
         * resource.getFilename() + "\"") .body(resource);
         */
    }

    public Resource loadFileAsResource(long id) throws Exception {
        try {

            Optional<Document> document = documentRepository.findById(id);
            if (!document.isPresent()) {
                throw new Exception("FILE_NOT_FOUND");
            }
            String path = "file:" + fileUploadPath + document.get().getPath();
            Resource resource = new UrlResource(new URI(path));
            if (resource.exists()) {
                return resource;
            } else {
                throw new Exception("FILE_NOT_FOUND");
            }
        } catch (MalformedURLException ex) {
            throw new Exception("FILE_NOT_FOUND");
        }
    }
}
