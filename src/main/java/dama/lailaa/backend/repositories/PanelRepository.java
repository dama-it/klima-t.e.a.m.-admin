package dama.lailaa.backend.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;

import dama.lailaa.backend.models.Panel;

public interface PanelRepository extends PagingAndSortingRepository<Panel, Long> {
    
   Page<Panel> findAll(Pageable pageable);

}
