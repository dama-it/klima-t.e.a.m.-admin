package dama.lailaa.backend.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import dama.lailaa.backend.models.Document;

public interface DocumentRepository extends PagingAndSortingRepository<Document, Long> {

}
