package dama.lailaa.backend.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import dama.lailaa.backend.models.Customer;

public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {
    
   

}
