package dama.lailaa.backend.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import dama.lailaa.backend.models.Project;

public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {

   @Query(value = "SELECT p FROM Project p " + 
                  " LEFT JOIN p.customer c " +
                  " WHERE (LOWER(p.name) LIKE LOWER(CONCAT('%',:fullText,'%')) OR :fullText = '') " + 
                  " OR (LOWER(c.name) LIKE LOWER(CONCAT('%',:fullText,'%')) OR :fullText = '') " + 
                  " OR (LOWER(c.address) LIKE LOWER(CONCAT('%',:fullText,'%')) OR :fullText = '') " + 
                  " OR (LOWER(c.phone) LIKE LOWER(CONCAT('%',:fullText,'%')) OR :fullText = '') " + 
                  " OR (LOWER(c.email) LIKE LOWER(CONCAT('%',:fullText,'%')) OR :fullText = '') " + 
                  " GROUP BY p.id")
   Page<Project> findAllSearch(Pageable pageable, @Param("fullText") String fullText);


}
