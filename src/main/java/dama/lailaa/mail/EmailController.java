package dama.lailaa.mail;

import java.io.UnsupportedEncodingException;
import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import dama.lailaa.mail.EmailService;

@RestController
@RequestMapping("app/email")
public class EmailController {

    @Autowired
    EmailService emailService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public MailDto sendSimpleMessage(Principal principal) throws UnsupportedEncodingException {

        try {
            emailService.sendSimpleMessage("", "", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}