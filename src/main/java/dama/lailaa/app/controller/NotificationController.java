package dama.lailaa.app.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dama.lailaa.app.models.Notification;
import dama.lailaa.app.repositories.NotificationRepository;
import dama.lailaa.application.exceptions.FormValidException;
import dama.lailaa.application.exceptions.ViewException;

@RestController
@RequestMapping("api/notifications")

public class NotificationController {
    
    @Autowired
    private NotificationRepository notificationRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Page<Notification> findAll(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters, Principal principal) throws Exception {
        
        Boolean notification_read = null;
            if(parameters.getFirst("notification_read") == "true"){
                notification_read = true;
            }
            if(parameters.getFirst("notification_read") == "false"){
                notification_read = false;
            }

            Boolean deleted = null;
            if(parameters.getFirst("deleted") == "true"){
                deleted = true;
            }
            if(parameters.getFirst("deleted") == "false"){
                deleted = false;
            }
        
        Page<Notification> page = notificationRepository.searchFull(pageable,
            parameters.getFirst("id") == null ? "" : parameters.getFirst("id"),
            parameters.getFirst("type") == null ? "" : parameters.getFirst("type"),
            parameters.getFirst("body") == null ? "" : parameters.getFirst("body"),
            notification_read,
            deleted);
    return page;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Notification findOne(@PathVariable long id, Principal principal) throws Exception {
        Optional<Notification> existingNotification = notificationRepository.findById(id);

        if (!existingNotification.isPresent()) {
            throw new ViewException("NOTIFICATION_DOES_NOT_EXIST");
        }

        return existingNotification.get();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Notification create(@RequestBody Notification notification) throws Exception {

        Notification n = new Notification();
        n.setId(notification.getId());
        n.setCreatedAt(notification.getCreatedAt());
        n.setUpdatedAt(notification.getUpdatedAt());
        n.setUserId(notification.getUserId());
        n.setType(notification.getType());
        n.setTypeId(notification.getTypeId());
        n.setBody(notification.getBody());
        n.setNotification_read(notification.isNotification_read());
        n.setDeleted(notification.isDeleted());

        notificationRepository.save(n);
        return n;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Notification update(@PathVariable long id, @RequestBody Notification notification) throws Exception {

        Optional<Notification> existingNotification = notificationRepository.findById(notification.getId());

        if(!existingNotification.isPresent()) {
            throw new Exception("NOTIFICATION_DOES_NOT_EXIST");
        }
        else {
            existingNotification.get().setId(notification.getId());
            existingNotification.get().setCreatedAt(notification.getCreatedAt());
            existingNotification.get().setUpdatedAt(notification.getUpdatedAt());
            existingNotification.get().setUserId(notification.getUserId());
            existingNotification.get().setType(notification.getType());
            existingNotification.get().setTypeId(notification.getTypeId());
            existingNotification.get().setBody(notification.getBody());
            existingNotification.get().setNotification_read(notification.isNotification_read());
            existingNotification.get().setDeleted(notification.isDeleted());

            notificationRepository.save(existingNotification.get());
        }

        return null;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Notification delete(@PathVariable long id) throws Exception {
        Optional<Notification> existingNotification = notificationRepository.findById(id);

        if (existingNotification.isPresent() == false) {
            throw new FormValidException("NOTIFICATION_DOES_NOT_EXIST");
        }
        notificationRepository.delete(existingNotification.get());

        return existingNotification.get();
    }
}
