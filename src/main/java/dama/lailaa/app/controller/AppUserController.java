package dama.lailaa.app.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dama.lailaa.application.exceptions.FormValidException;
import dama.lailaa.application.exceptions.ViewException;
import dama.lailaa.app.models.AppUser;
import dama.lailaa.app.repositories.AppUserRepository;



@RestController
@RequestMapping("api/appUsers")
public class AppUserController {
    
    @Autowired
    private AppUserRepository appUserRepository;
    
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Page<AppUser> findAll(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters,
        Principal principal) throws Exception {

        Boolean active = null;
        if(parameters.getFirst("active") == "true"){
            active = true;
        }
        if(parameters.getFirst("active") == "false"){
            active = false;
        }

        Boolean calibration = null;
        if(parameters.getFirst("calibration") == "true"){
            calibration = true;
        }
        if(parameters.getFirst("calibration") == "false"){
            calibration = false;
        }

        Page<AppUser> page = appUserRepository.searchFull(pageable,
        parameters.getFirst("id") == null ? "" : parameters.getFirst("id"),
        parameters.getFirst("username") == null ? "" : parameters.getFirst("username"),
        parameters.getFirst("firstName") == null ? "" : parameters.getFirst("firstName"),
        parameters.getFirst("lastName") == null ? "" : parameters.getFirst("lastName"),
        active,
        calibration);
        
        return page;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AppUser findOne(@PathVariable long id, Principal principal) throws Exception {

        Optional<AppUser> existingUser = appUserRepository.findById(id);

        if (existingUser.isPresent() == false) {
            throw new ViewException("APP_USER_DOES_NOT_EXIST");
        }

        return existingUser.get();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public AppUser create(@RequestBody AppUser appUser, Principal principal) throws Exception {

        AppUser a = new AppUser();
        a.setId(appUser.getId());
        a.setEmail(appUser.getEmail());
        a.setUsername(appUser.getUsername());
        a.setPassword(appUser.getPassword());
        a.setFirstName(appUser.getFirstName());
        a.setLastName(appUser.getLastName());
        a.setCreatedAt(appUser.getCreatedAt());
        a.setActive(appUser.isActive());

        appUserRepository.save(a);

        return a;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public AppUser update(@PathVariable long id, @RequestBody AppUser appUser, Principal principal) throws Exception {

        Optional<AppUser> user = appUserRepository.findById(appUser.getId());

        if (!user.isPresent()) {
            throw new ViewException("APP_USER_DOES_NOT_EXIST");
        }

        else {
            AppUser a = user.get();
            a.setId(appUser.getId());
            a.setEmail(appUser.getEmail());
            a.setUsername(appUser.getUsername());
            a.setPasswordEncrypted(appUser.getPassword());
            a.setFirstName(appUser.getFirstName());
            a.setLastName(appUser.getLastName());
            a.setCreatedAt(appUser.getCreatedAt());
            a.setActive(appUser.isActive());

            appUserRepository.save(a);
        }

        return null;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public AppUser delete(@PathVariable long id, Principal principal) throws Exception {
        Optional<AppUser> existingAppUser = appUserRepository.findById(id);
        if (!existingAppUser.isPresent()) {
            throw new FormValidException("APP_USER_DOES_NOT_EXIST");
        }
        appUserRepository.delete(existingAppUser.get());
        return existingAppUser.get();
    }
    
} 
