package dama.lailaa.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Optional;

import dama.lailaa.app.models.Order;
import dama.lailaa.app.repositories.OrderRepository;
import dama.lailaa.app.repositories.AppUserRepository;
import dama.lailaa.application.exceptions.FormValidException;
import dama.lailaa.application.exceptions.ViewException;

@RestController
@RequestMapping("api/orders")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public Page<Order> findAll(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters,
            Principal principal) throws Exception {
        Page<Order> page = orderRepository.searchFull(pageable,
                parameters.getFirst("id") == null ? "" : parameters.getFirst("id"),
                parameters.getFirst("type") == null ? "" : parameters.getFirst("type"),
                parameters.getFirst("orderNumber") == null ? "" : parameters.getFirst("orderNumber"),
                parameters.getFirst("createdAt") == null ? "" : parameters.getFirst("createdAt"),
                parameters.getFirst("startInstallationDate") == null ? ""
                        : parameters.getFirst("startInstallationDate"),
                parameters.getFirst("endInstallationDate") == null ? "" : parameters.getFirst("endInstallationDate"));
        return page;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Order findOne(@PathVariable long id, Principal principal) throws Exception {
        Optional<Order> existingOrder = orderRepository.findById(id);

        if (existingOrder.isPresent() == false) {
            throw new ViewException("ORDER_DOES_NOT_EXIST");
        }

        return existingOrder.get();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Order create(@RequestBody Order order) throws Exception {

        Order o = new Order();
        o.setId(order.getId());
        o.setType(order.getType());
        o.setOrderNumber(order.getOrderNumber());
        o.setStatus(order.getStatus());
        o.setOrderDate(order.getOrderDate());
        o.setStartInstallationDate(order.getStartInstallationDate());
        o.setEndInstallationDate(order.getEndInstallationDate());
        // o.setCreatedAt(order.getCreatedAt());
        // o.setWorker(order.getWorker());
        // o.setClient(order.getClient());
        o.setManual(order.getManual());
        o.setNotes1(order.getNotes1());
        o.setNotes2(order.getNotes2());

        orderRepository.save(o);

        return o;
    }

}
