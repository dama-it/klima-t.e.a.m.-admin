package dama.lailaa.app.repositories;

import dama.lailaa.app.models.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {

        @Query(value = "SELECT u FROM Order u " +
                        "WHERE " +
                        "(LOWER(u.id) LIKE LOWER(CONCAT('%',:id,'%')) OR :id = '') " +
                        "AND (LOWER(u.type) LIKE LOWER(CONCAT('%',:type,'%')) OR :type = '') " +
                        "AND (LOWER(u.orderNumber) LIKE LOWER(CONCAT('%',:orderNumber,'%')) OR :orderNumber = '') " +
                        "AND (LOWER(u.createdAt) LIKE LOWER(CONCAT('%',:createdAt,'%')) OR :createdAt = '') " +
                        "AND (LOWER(u.endInstallationDate) LIKE LOWER(CONCAT('%',:endInstallationDate,'%')) OR :endInstallationDate = '') "
                        +
                        "AND (LOWER(u.startInstallationDate) LIKE LOWER(CONCAT('%',:startInstallationDate,'%')) OR :startInstallationDate = '') "
                        +
                        "GROUP BY u.id")
        Page<Order> searchFull(
                        Pageable pageable,
                        @Param("id") String id,
                        @Param("type") String type,
                        @Param("orderNumber") String orderNumber,
                        @Param("createdAt") String createdAt,
                        @Param("startInstallationDate") String startInstallationDate,
                        @Param("endInstallationDate") String endInstallationDate);
}
