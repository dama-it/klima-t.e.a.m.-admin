package dama.lailaa.app.repositories;

import dama.lailaa.app.models.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;

public interface AppUserRepository extends PagingAndSortingRepository<AppUser, Long> {

    AppUser findByUsername(String username); 

    @Query(value = "SELECT u FROM AppUser u " +
    "WHERE " +
    "(LOWER(u.id) LIKE LOWER(CONCAT('%',:id,'%')) OR :id = '') " +
    "AND (LOWER(u.username) LIKE LOWER(CONCAT('%',:username,'%')) OR :username = '') " +
    "AND (LOWER(u.firstName) LIKE LOWER(CONCAT('%',:firstName,'%')) OR :firstName = '') " +
    "AND (LOWER(u.lastName) LIKE LOWER(CONCAT('%',:lastName,'%')) OR :lastName = '') " +
    "AND ((u.active = :active) OR (:active IS NULL)) " +
    "AND ((u.calibration = :calibration) OR (:calibration IS NULL)) " +
    "GROUP BY u.id")
    Page<AppUser> searchFull(
        Pageable pageable,
        @Param("id") String id,
        @Param("username") String username,
        @Param("firstName") String firstName,
        @Param("lastName") String lastName,
        @Param("active") Boolean active,
        @Param("calibration") Boolean calibration
    );
    
}