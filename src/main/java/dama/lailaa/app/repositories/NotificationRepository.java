package dama.lailaa.app.repositories;

import dama.lailaa.app.models.Notification;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;

public interface NotificationRepository extends PagingAndSortingRepository<Notification, Long> {
 
    @Query(value = "SELECT u FROM Notification u " +
    "WHERE " +
    "(LOWER(u.id) LIKE LOWER(CONCAT('%',:id,'%')) OR :id = '') " +
    "AND (LOWER(u.type) LIKE LOWER(CONCAT('%',:type,'%')) OR :type = '') " +
    "AND (LOWER(u.body) LIKE LOWER(CONCAT('%',:body,'%')) OR :body = '') " +
    "AND ((u.notification_read = :notification_read) OR (:notification_read IS NULL)) " +
    "AND ((u.deleted = :deleted) OR (:deleted IS NULL)) " +
    "GROUP BY u.id")
    Page<Notification> searchFull(
        Pageable pageable,
        @Param("id") String id,
        @Param("type") String type,
        @Param("body") String body,
        @Param("notification_read") Boolean notification_read,
        @Param("deleted") Boolean deleted
    );

}
