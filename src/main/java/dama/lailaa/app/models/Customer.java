package dama.lailaa.app.models;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;

    private String title;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    private String address;

    private String zip;

    private String city;

    private String country;

}