package dama.lailaa.app.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.*;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.Pattern;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "app_user")
public class AppUser {

    private static final PasswordEncoder pwEncoder = new BCryptPasswordEncoder();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Email(message = "EMAIL_FORMAT")
    private String email;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Length(min = 3, message = "FIELD_MUST_HAVE_AT_LEAST_3_CHARACTERS")
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Length(min = 6, message = "FIELD_MUST_HAVE_AT_LEAST_6_CHARACTERS")
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,128}$", message = "PW_MIN_6_MAX_128_1_UPPER_1_LOWER_1_NUMERIC")
    private String password;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Length(min = 2, message = "FIELD_MUST_HAVE_AT_LEAST_2_CHARACTERS")
    private String firstName;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Length(min = 2, message = "FIELD_MUST_HAVE_AT_LEAST_2_CHARACTERS")
    private String lastName;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    private Date createdAt;

    private int points = 0;

    private boolean active = true;

    private boolean calibration = false;

    @PrePersist
    public void prePersist() {
        password = pwEncoder.encode(password);
    }

    public void setPasswordEncrypted(String password) {
        this.password = pwEncoder.encode(password);
    }

}