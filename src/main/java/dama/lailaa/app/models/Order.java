package dama.lailaa.app.models;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "installation_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;

    private String type;

    private String orderNumber;

    private String status;

    private Date orderDate;

    private Date startInstallationDate;

    private Date endInstallationDate;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    @OneToOne
    private Worker worker;

    @ManyToOne
    private Customer client;

    private String manual;

    private String notes1;

    private String notes2;

}
