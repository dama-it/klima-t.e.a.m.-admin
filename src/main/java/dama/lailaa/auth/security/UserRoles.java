package dama.lailaa.auth.security;

public class UserRoles {
    public static final String ROLE_ADMIN = "ROLE_ADMIN"; // should be in DB -> only predefined role
    public static final String ROLE_GLOBAL = "ROLE_GLOBAL_"; // -> Global role prefix -> define permissions for all users of all clients -> can only be set by an IS_ADMIN user
    public static final String ROLE_CLIENT_GLOBAL = "ROLE_CLIENT_GLOBAL_"; // -> Global role prefix -> define permissions for all user of a client! -> can only be set by IS_ADMIN or IS_CLIENT_ADMIN users
}
