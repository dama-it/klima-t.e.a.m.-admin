package dama.lailaa.auth.security.permissions;

import java.lang.reflect.Field;
import java.util.ArrayList;
import dama.lailaa.auth.models.UserPermission;

public class Permissions {
    /**
     * Returns possible enum constants as string array
     * @return
     */
     public static ArrayList<UserPermission> getPermissions(Class<? extends BasePermission> permissionClass){
        ArrayList<UserPermission> permissions = new ArrayList<>();

        for(Field field : permissionClass.getFields()){
            try {
                String value = (String)field.get(permissionClass);
                permissions.add(new UserPermission(value,value, getCategory(value)));

            }catch(Exception e){
                // do nothing
                System.out.println("ERROR: getAllPermissions - read field error" );
                System.out.println(e.getMessage());
                return null;
            }
        }
        return permissions;
    }

    public static String getCategory(String permissionValue) {
        switch(permissionValue) {
            case InternalPermission.IS_ADMIN: 
            case ModeratorRolePermission.IS_SUPER_MODERATOR:
            case ClientPermission.IS_CLIENT_ADMIN:
                return "ADMINISTRATION";

            default: return "OTHER";
        }
    }
}
 