package dama.lailaa.auth.security.permissions;

public class ModeratorRolePermission extends BasePermission {
    public static final String IS_SUPER_MODERATOR = "IS_SUPER_MODERATOR";
    public static final String EDIT_COMMENT = "EDIT_COMMENT";
    public static final String REMOVE_COMMENT = "REMOVE_COMMENT";
    public static final String REMOVE_TOPIC = "REMOVE_TOPIC";
    public static final String EDIT_TOPIC = "EDIT_TOPIC";
}
