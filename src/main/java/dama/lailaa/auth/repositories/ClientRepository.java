package dama.lailaa.auth.repositories;

import dama.lailaa.auth.models.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {

    @Query(value = "SELECT c FROM Client c WHERE " +
            "(LOWER(c.name) LIKE LOWER(CONCAT('%',:name,'%')) OR :name = '') " +
            "AND (LOWER(CONCAT(c.name)) LIKE LOWER(CONCAT('%',:fullText,'%')) OR :fullText = '') " +
            "AND (c.id = :clientId OR :clientId = 0) " +
            "AND c.active = :active")
    Page<Client> searchFull(
            Pageable pageable,
            @Param("fullText") String fullText,
            @Param("name") String name,
            @Param("active") boolean active,
            @Param("clientId") long clientId
    );

    Client findByName(String name);
}
