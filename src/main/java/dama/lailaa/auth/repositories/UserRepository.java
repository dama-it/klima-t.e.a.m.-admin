package dama.lailaa.auth.repositories;

import dama.lailaa.auth.models.Client;
import dama.lailaa.auth.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByUsername(String username);

    User findByIdAndClient(long id, Client client);

    @Query(value = "SELECT u FROM User u " +
            "LEFT JOIN u.userRoles ur " +
            "WHERE " +
            "(LOWER(CONCAT(u.username, u.lastName, u.firstName)) LIKE LOWER(CONCAT('%',:fullText,'%')) OR :fullText = '') " +
            "AND (LOWER(u.username) LIKE LOWER(CONCAT('%',:username,'%')) OR :username = '') " +
            "AND (LOWER(u.lastName) LIKE LOWER(CONCAT('%',:lastName,'%')) OR :lastName = '') " +
            "AND (LOWER(u.firstName) LIKE LOWER(CONCAT('%',:firstName,'%')) OR :firstName = '') " +
            "AND (LOWER(u.email) LIKE LOWER(CONCAT('%',:email,'%')) OR :email = '') " +
            "AND (LOWER(ur.name) LIKE LOWER(CONCAT('%',:userRoleName,'%')) OR :userRoleName = '') " +
            "AND (ur.id = :userRoleId OR :userRoleId = 0) " +
            "AND (u.client.id = :clientId OR :clientId = 0) " +
            "AND u.active = :active " +
            "GROUP BY u.id")
    Page<User> searchFull(
            Pageable pageable,
            @Param("fullText") String fullText,
            @Param("username") String username,
            @Param("lastName") String lastName,
            @Param("firstName") String firstName,
            @Param("email") String email,
            @Param("userRoleName") String userRoleName,
            @Param("userRoleId") long userRoleId,
            @Param("clientId") long clientId,
            @Param("active") boolean active
    );

}
