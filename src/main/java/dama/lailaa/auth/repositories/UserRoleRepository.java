package dama.lailaa.auth.repositories;

import dama.lailaa.auth.models.Client;
import dama.lailaa.auth.models.UserRole;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRoleRepository extends PagingAndSortingRepository<UserRole, Long> {
    UserRole findByName(String name);
    UserRole findByNameAndClientAndDeletedIsFalse(String name, Client client);
    UserRole findByNameAndClientAndDeletedIsFalseAndIdIsNot(String name, Client client, int id);

    Page<UserRole> findByNameContainingIgnoreCase(
            Pageable pageable,
            String name
    );

    ArrayList<UserRole> findByNameContainingIgnoreCaseAndDeletedAndClient(
            String name,
            boolean deleted,
            Client client
    );

    @Query(value = "SELECT r FROM UserRole r " +
            "WHERE " +
            "((LOWER(r.name) LIKE LOWER(CONCAT('%',:name, '%')) AND r.deleted = 0 AND client_id = :clientId AND r.name not like 'ROLE_CLIENT_GLOBAL_%') or " +
            "((LOWER(r.name) LIKE LOWER(CONCAT('%',:name, '%')) AND (r.name LIKE CONCAT('ROLE_CLIENT_GLOBAL','%')  AND r.deleted = 0))))")

    Page<UserRole> findRolesForClientAdmins(
            Pageable pageable,
            @Param("name") String name,
            @Param("clientId") long clientId
    );

    @Query(value = "SELECT r FROM UserRole r " +
            "WHERE " +
            "(LOWER(r.name) LIKE LOWER(CONCAT('%',:name, '%')) OR :name = '') " +
            "AND r.deleted = 0 " + 
            "AND (r.client.id = :clientId OR :clientId = 0)")
    Page<UserRole> search(
            Pageable pageable,
            @Param("name") String name,
            @Param("clientId") long clientId
    );


}
