package dama.lailaa.auth.controller;

import dama.lailaa.auth.models.IdentityDto;
import dama.lailaa.auth.models.IdentityProjection;
import dama.lailaa.auth.models.User;
import dama.lailaa.auth.models.UserRole;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.repositories.UserRoleRepository;
import dama.lailaa.auth.security.UserRoles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Set;

@RestController

@RequestMapping("api/identity")
// RequiredArgsConstructor(onConstructor = @__(@Autowired) )
public class IdentityController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private ProjectionFactory projectionFactory;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    public IdentityDto getIdentity(Principal principal) throws Exception {
        String username = principal.getName();
        User currentUser = userRepository.findByUsername(username);

        // MERGE USER-ROLES WITH GLOBAL ROLES
        ArrayList<UserRole> globalRoles = userRoleRepository
                .findByNameContainingIgnoreCaseAndDeletedAndClient(UserRoles.ROLE_GLOBAL, false, null);

        if (globalRoles != null && globalRoles.size() > 0) {
            Set<UserRole> roles = currentUser.getUserRoles();
            roles.addAll(roles);
            currentUser.setUserRoles(roles);
        }

        // MERGE USER-ROLES WITH GLOBAL CLIENT-ROLES WITH USER IS HAS A CLIENT
        if (currentUser.getClient() != null) {
            ArrayList<UserRole> globalClientRoles = userRoleRepository
                    .findByNameContainingIgnoreCaseAndDeletedAndClient(UserRoles.ROLE_CLIENT_GLOBAL, false,
                            currentUser.getClient());
            if (globalClientRoles != null && globalClientRoles.size() > 0) {
                globalRoles.addAll(globalClientRoles);
            }
        }

        if (globalRoles.size() > 0) {
            Set<UserRole> roles = currentUser.getUserRoles();
            for (UserRole r : globalRoles) {
                if (!roles.contains(r)) {
                    roles.add(r);
                }
            }
            currentUser.setUserRoles(roles);
        }

        IdentityDto identityContainer = new IdentityDto();
        IdentityProjection projection = projectionFactory.createProjection(IdentityProjection.class, currentUser);
        identityContainer.setIdentity(projection);
        return identityContainer;
    }
}
