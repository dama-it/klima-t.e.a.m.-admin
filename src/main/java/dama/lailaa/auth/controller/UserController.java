package dama.lailaa.auth.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dama.lailaa.application.exceptions.FormValidException;
import dama.lailaa.application.exceptions.ViewException;
import dama.lailaa.auth.models.Client;
import dama.lailaa.auth.models.User;
import dama.lailaa.auth.models.UserRole;
import dama.lailaa.auth.repositories.ClientRepository;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.repositories.UserRoleRepository;
import dama.lailaa.auth.security.UserRoles;
import dama.lailaa.auth.security.permissions.ClientPermission;
import dama.lailaa.auth.security.permissions.InternalPermission;

@RestController
@RequestMapping("api/users")
// doesn't work with PreAuthorize?
// @BasePathAwareController
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private ClientRepository clientRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public Page<User> findAll(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters,
            Principal principal) throws Exception {

        User currentUser = userRepository.findByUsername(principal.getName());
        Long clientId = parameters.getFirst("clientPk") == null ? Long.valueOf(0)
                : Long.valueOf(parameters.getFirst("clientPk"));
        if (!currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            clientId = currentUser.getClient().getId();
        }

        Page<User> page = userRepository.searchFull(pageable,
                parameters.getFirst("fullText") == null ? "" : parameters.getFirst("fullText"),
                parameters.getFirst("username") == null ? "" : parameters.getFirst("username"),
                parameters.getFirst("lastName") == null ? "" : parameters.getFirst("lastName"),
                parameters.getFirst("firstName") == null ? "" : parameters.getFirst("firstName"),
                parameters.getFirst("email") == null ? "" : parameters.getFirst("email"),
                parameters.getFirst("userRole") == null ? "" : parameters.getFirst("userRole"),
                parameters.getFirst("userRolePk") == null ? (long) 0 : Long.valueOf(parameters.getFirst("userRolePk")),
                clientId, parameters.getFirst("active") == null || parameters.getFirst("active").equals("true"));
        return page;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public User findOne(@PathVariable long id, Principal principal) throws Exception {

        User currentUser = userRepository.findByUsername(principal.getName());
        User user = null;

        // if current user is admin -> get every user
        if (currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            Optional<User> userOpt = userRepository.findById(id);
            if (userOpt.isPresent()) {
                user = userOpt.get();
            }
        }

        // if current user is client-admin -> get only user of its client
        else if (currentUser.hasPermission(ClientPermission.IS_CLIENT_ADMIN)) {
            user = userRepository.findByIdAndClient(id, currentUser.getClient());
        }

        // if current user is simple user - only get himself
        else {
            if (currentUser.getId() == id) {
                Optional<User> userOpt = userRepository.findById(id);
                if (userOpt.isPresent()) {
                    user = userOpt.get();
                }
            }
        }

        if (user == null) {
            throw new ViewException("INVALID_USERROLE");
        }

        user.setUserRoles(user.getUserRoles());
        return user;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public User create(@RequestBody User user, Principal principal) throws Exception {
        // perform user exists checks

        User existingUser = userRepository.findByUsername(user.getUsername());

        Optional<Client> client = clientRepository.findById((long) 1);
        if (client.isPresent()) {
            user.setClient(client.get());
        }

        if (existingUser != null) {
            throw new FormValidException("USER_ALREADY_EXISTS");
        }

        List<Long> userRoleIds = user.getUserRoleIds();
        HashSet<UserRole> userRoles = new HashSet<>(0);

        for (long userRolesId : userRoleIds) {
            Optional<UserRole> userRoleOpt = userRoleRepository.findById(userRolesId);
            if (!userRoleOpt.isPresent()) {
                continue;
            }
            userRoles.add(userRoleOpt.get());
        }
        user.setUserRoles(userRoles);
        userRepository.save(user);
        return user;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public User update(@PathVariable long id, @RequestBody User user, Principal principal) throws Exception {

        User currentUser = userRepository.findByUsername(principal.getName());
        // perform user exists checks
        Optional<User> existingUserOpt = userRepository.findById(id);
        if (!existingUserOpt.isPresent()) {
            throw new Exception("USER_DOES_NOT_EXIST");
        }

        // if current user is admin -> use given client if exists
       /* if (currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            Optional<Client> clientOpt = clientRepository.findById(user.getClient().getId());
            if (clientOpt.isPresent()) {
                user.setClient(clientOpt.get());
            } else {
                throw new FormValidException("CLIENT_DOES_NOT_EXIST");
            }
        } else {
            // current user is client-admin -> create user for current user client
            if (user.getClient().getId() != currentUser.getClient().getId()) {
                throw new FormValidException("PERMISSION_DENIED");
            }
        }*/

        List<Long> userRoleIds = user.getUserRoleIds();
        HashSet<UserRole> userRoles = new HashSet<>(0);

        for (long userRolesId : userRoleIds) {
            Optional<UserRole> userRoleOpt = userRoleRepository.findById(userRolesId);
            if (!userRoleOpt.isPresent()) {
                continue;
            }
            userRoles.add(userRoleOpt.get());
        }

        user.setUserRoles(userRoles);
        user.setUserRoles(userRoles);
        existingUserOpt.get().update(user);
        userRepository.save(existingUserOpt.get());
        return existingUserOpt.get();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public User delete(@PathVariable long id, Principal principal) throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());
        // perform user exists checks

        Optional<User> existingUserOpt = userRepository.findById(id);
        if (!existingUserOpt.isPresent()) {
            throw new FormValidException("USER_DOES_NOT_EXIST");
        }

        if (!currentUser.hasUserRole(UserRoles.ROLE_ADMIN)
                && existingUserOpt.get().getClient().getId() != currentUser.getClient().getId()) {
            throw new FormValidException("PERMISSION_DENIED");
        }

        existingUserOpt.get().setActive(false);
        userRepository.save(existingUserOpt.get());
        return existingUserOpt.get();
    }
}
