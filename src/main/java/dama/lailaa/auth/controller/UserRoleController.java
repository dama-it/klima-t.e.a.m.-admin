package dama.lailaa.auth.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dama.lailaa.application.exceptions.FormValidException;
import dama.lailaa.auth.models.Client;
import dama.lailaa.auth.models.User;
import dama.lailaa.auth.models.UserRole;
import dama.lailaa.auth.repositories.ClientRepository;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.repositories.UserRoleRepository;
import dama.lailaa.auth.security.UserRoles;
import dama.lailaa.auth.security.permissions.ClientPermission;
import dama.lailaa.auth.security.permissions.InternalPermission;
import dama.lailaa.auth.services.CurrentUserServiceImpl;

@RestController
@RequestMapping("api/userRoles")
// doesn't work with PreAuthorize?
// @BasePathAwareController
public class UserRoleController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    CurrentUserServiceImpl userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public Page<UserRole> findAll(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters,
            Principal principal) throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());
        Client client = currentUser.getClient();

        // return all not deleted roles
        if (userService.hasPermission(principal, InternalPermission.IS_ADMIN)) {
            return userRoleRepository.search(pageable,
                    parameters.getFirst("name") == null ? "" : parameters.getFirst("name"),
                    parameters.getFirst("clientPk") == null ? Long.valueOf(0)
                            : Long.valueOf(parameters.getFirst("clientPk")));
        }

        // return all global client roles and custom roles for client
        if (userService.hasPermission(principal, ClientPermission.IS_CLIENT_ADMIN)) {
            return userRoleRepository.findRolesForClientAdmins(pageable,
                    parameters.getFirst("name") == null ? "" : parameters.getFirst("name"), client.getId());
        }

        // if no admin - return empty array
        return new PageImpl<>(new ArrayList<>(), pageable, 0);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public UserRole findOne(@PathVariable long id, Principal principal) throws Exception {
        Optional<UserRole> role = userRoleRepository.findById(id);
        User currentUser = userRepository.findByUsername(principal.getName());

        if (!role.isPresent()) {
            throw new Exception("USERROLE_DOES_NOT_EXIST");
        }

        if (!userService.hasPermission(principal, InternalPermission.IS_ADMIN)) {
            if (role.get().getClient() != null && role.get().getClient().getId() != currentUser.getClient().getId()) {
                throw new Exception("PERMISSION_DENIED");
            }
            if (role.get().getClient() == null && (!role.get().getName().startsWith(UserRoles.ROLE_CLIENT_GLOBAL)
                    || !role.get().getName().startsWith(UserRoles.ROLE_GLOBAL))) {
                throw new Exception("PERMISSION_DENIED");
            }
        }

        return role.get();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public UserRole create(@RequestBody UserRole userRole, Principal principal) throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());
        if (!currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            UserRole existingUserRole = userRoleRepository.findByNameAndClientAndDeletedIsFalse(userRole.getName(),
                    currentUser.getClient());

            if (existingUserRole != null) {
                throw new FormValidException("USERROLE_ALREADY_EXISTS");
            }
        }

        // check reserved role-names
        if (!currentUser.hasUserRole(UserRoles.ROLE_ADMIN) && (userRole.getName().contains(UserRoles.ROLE_GLOBAL))) {
            throw new FormValidException("USERROLE_NAME_NOT_VALID");
        }

        // is current user is admin - use selected client-role - otherwesie use client
        // of
        // current user
        if (currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            if (userRole.getClient() != null) {
                Optional<Client> clientOpt = clientRepository.findById(userRole.getClient().getId());
                if (clientOpt.isPresent()) {
                    userRole.setClient(clientOpt.get());
                }
            }
        } else {
            userRole.setClient(currentUser.getClient());
        }

        userRoleRepository.save(userRole);

        return userRole;

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public UserRole update(@PathVariable long id, @RequestBody UserRole userRole, Principal principal)
            throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());
        // perform user exists checks
        Optional<UserRole> existingUserRoleOpt = userRoleRepository.findById(id);
        if (!existingUserRoleOpt.isPresent()) {
            throw new FormValidException("USERROLE_DOES_NOT_EXIST");
        }

        // check reserved role-names
        if (!currentUser.hasUserRole(UserRoles.ROLE_ADMIN)
                && existingUserRoleOpt.get().getName().contains(UserRoles.ROLE_GLOBAL)) {
            throw new FormValidException("PERMISSION_DENIED_ROLE_NAME_RESERVED");
        }

        // check ROLE_ADMIN -> ROLE_ADMIN is not editable
        if (existingUserRoleOpt.get().getName().equals(UserRoles.ROLE_ADMIN)) {
            throw new FormValidException("PERMISSION_DENIED_ROLE_ADMIN_NOT_EDITABLE");
        }

        if (!currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            UserRole existingUserRoleWithName = userRoleRepository
                    .findByNameAndClientAndDeletedIsFalse(userRole.getName(), currentUser.getClient());
            if (existingUserRoleWithName != null
                    && existingUserRoleOpt.get().getId() != existingUserRoleWithName.getId()) {
                throw new FormValidException("USERROLE_ALREADY_EXISTS");
            }
        }

        if (currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            if (userRole.getClient() != null) {
                Optional<Client> clientOpt = clientRepository.findById(userRole.getClient().getId());
                if (clientOpt.isPresent()) {
                    userRole.setClient(clientOpt.get());
                } else {
                    userRole.setClient(null);
                }
            }
        } else {
            userRole.setClient(currentUser.getClient());
        }

        existingUserRoleOpt.get().update(userRole);
        userRoleRepository.save(existingUserRoleOpt.get());
        return existingUserRoleOpt.get();

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public UserRole delete(@PathVariable long id, Principal principal) throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());
        // perform user exists checks
        Optional<UserRole> existingUserRoleOpt = userRoleRepository.findById(id);
        if (!existingUserRoleOpt.isPresent()) {
            throw new FormValidException("USERROLE_DOES_NOT_EXIST");
        }

        // check ROLE_ADMIN -> ROLE_ADMIN is not editable
        if (existingUserRoleOpt.get().getName().equals(UserRoles.ROLE_ADMIN)) {
            throw new FormValidException("PERMISSION_DENIED_ROLE_ADMIN_NOT_DELETEABLE");
        }

        // check ROLE_ADMIN -> ROLE_ADMIN is not editable
        if (existingUserRoleOpt.get().getName().startsWith(UserRoles.ROLE_GLOBAL)
                && !currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            throw new FormValidException("PERMISSION_DENIED_ROLE_GLOBAL_NOT_DELETEABLE");
        }

        // check ROLE_CLIENT_GLOBAL_XY -> only user with permission IS_CLIENT_ADMIN or
        // IS_ADMIN should delete this role
        if (existingUserRoleOpt.get().getName().startsWith(UserRoles.ROLE_CLIENT_GLOBAL)) {
            if (!currentUser.hasUserRole(UserRoles.ROLE_ADMIN)
                    && !currentUser.hasPermission(ClientPermission.IS_CLIENT_ADMIN)) {
                throw new FormValidException("PERMISSION_DENIED_ROLE_CLIENT_GLOBAL_NOT_DELETEABLE_BY_USER");
            }

            if (currentUser.hasPermission(ClientPermission.IS_CLIENT_ADMIN) && currentUser.getClient() != null
                    && currentUser.getClient().getId() != existingUserRoleOpt.get().getClient().getId()) {
                throw new FormValidException("PERMISSION_DENIED_ROLE_CLIENT_GLOBAL_NOT_DELETEABLE_BY_USER");
            }
        }

        existingUserRoleOpt.get().setDeleted(true);
        userRoleRepository.save(existingUserRoleOpt.get());
        return existingUserRoleOpt.get();

    }
}
