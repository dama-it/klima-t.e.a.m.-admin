package dama.lailaa.auth.controller;

import java.security.Principal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import dama.lailaa.application.exceptions.FormValidException;
import dama.lailaa.application.exceptions.ViewException;
import dama.lailaa.auth.models.Client;
import dama.lailaa.auth.models.User;
import dama.lailaa.auth.repositories.ClientRepository;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.security.permissions.ClientPermission;
import dama.lailaa.auth.security.permissions.InternalPermission;

@RestController
@RequestMapping("api/clients")
// doesn't work with PreAuthorize?
// @BasePathAwareController
public class ClientController {
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public Page<Client> findAll(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters,
            Principal principal) throws Exception {

        User currentUser = userRepository.findByUsername(principal.getName());
        Long clientId = parameters.getFirst("clientPk") == null ? Long.valueOf(0)
                : Long.valueOf(parameters.getFirst("clientPk"));

        if (!currentUser.hasPermission(InternalPermission.IS_ADMIN)) {
            clientId = currentUser.getClient().getId();
        }
        return clientRepository.searchFull(pageable,
                parameters.getFirst("fullText") == null ? "" : parameters.getFirst("fullText"),
                parameters.getFirst("name") == null ? "" : parameters.getFirst("name"),
                parameters.getFirst("active") == null || parameters.getFirst("active").equals("true"), clientId);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@currentUserServiceImpl.hasOneOfPermissions(#principal, new String[]{'"
            + ClientPermission.IS_CLIENT_ADMIN + "','" + InternalPermission.IS_ADMIN + "'})")
    @ResponseBody
    public Client findOne(@PathVariable long id, Principal principal) throws Exception {
        Optional<Client> existingClient = clientRepository.findById(id);

        if (existingClient.isPresent() == false) {
            throw new ViewException("CLIENT_DOES_NOT_EXIST");
        }

        User currentUser = userRepository.findByUsername(principal.getName());

        if (!currentUser.hasPermission(InternalPermission.IS_ADMIN)
                && currentUser.getClient().getId() != existingClient.get().getId()) {
            throw new ViewException("PERMISSION_DENIED");
        }
        return existingClient.get();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + InternalPermission.IS_ADMIN + "')")
    @ResponseBody
    public Client create(@RequestBody Client client, Principal principal) throws Exception {
        Client existingClient = clientRepository.findByName(client.getName());
        if (existingClient != null) {
            throw new FormValidException("CLIENT_ALREADY_EXISTS");
        }
        clientRepository.save(client);
        return client;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + InternalPermission.IS_ADMIN + "')")
    @ResponseBody
    public Client update(@PathVariable long id, @RequestBody Client client, Principal principal) throws Exception {
        Optional<Client> existingClient = clientRepository.findById(id);
        if (existingClient.isPresent() == false) {
            throw new FormValidException("CLIENT_DOES_NOT_EXIST");
        }
        existingClient.get().update(client);
        clientRepository.save(existingClient.get());
        return existingClient.get();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + InternalPermission.IS_ADMIN + "')")
    @ResponseBody
    public Client delete(@PathVariable long id, Principal principal) throws Exception {
        Optional<Client> existingClient = clientRepository.findById(id);
        if (existingClient.isPresent() == false) {
            throw new FormValidException("CLIENT_DOES_NOT_EXIST");
        }
        existingClient.get().setActive(false);
        clientRepository.save(existingClient.get());
        return existingClient.get();
    }
}
