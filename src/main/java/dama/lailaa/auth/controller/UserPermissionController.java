package dama.lailaa.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import dama.lailaa.application.exceptions.ViewException;
import dama.lailaa.auth.models.User;
import dama.lailaa.auth.models.UserPermission;
import dama.lailaa.auth.repositories.UserRepository;

import dama.lailaa.auth.security.UserRoles;
import dama.lailaa.auth.security.permissions.ClientPermission;
import dama.lailaa.auth.security.permissions.InternalPermission;
import dama.lailaa.auth.security.permissions.Permissions;
import dama.lailaa.auth.security.permissions.RolePermission;

import javax.validation.constraints.NotNull;
import java.security.Principal;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/userPermissions")
// doesn't work with PreAuthorize?
// @BasePathAwareController
public class UserPermissionController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + ClientPermission.IS_CLIENT_ADMIN + "')")
    @ResponseBody
    public Page<UserPermission> findAll(Pageable pageable, Principal principal) throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());
        List<UserPermission> permissions = this.getAllPermissions(currentUser);

        int start = (int) pageable.getOffset();
        int end = (start + pageable.getPageSize()) > permissions.size() ? permissions.size()
                : (start + pageable.getPageSize());
        Page<UserPermission> permissionsPage = new PageImpl<UserPermission>(permissions.subList(start, end), pageable,
                permissions.size());

        return permissionsPage;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @PreAuthorize("@currentUserServiceImpl.hasPermission(#principal, '" + ClientPermission.IS_CLIENT_ADMIN + "')")
    @ResponseBody
    public UserPermission findOne(@PathVariable String id, Principal principal) throws Exception {

        User currentUser = userRepository.findByUsername(principal.getName());
        List<UserPermission> permissions = this.getAllPermissions(currentUser);
        for (UserPermission up : permissions) {
            if (up.getId().equals(id)) {
                return up;
            }
        }
        throw new ViewException("USERPERMISSION_DOESNT_EXIST");
    }

    private List<UserPermission> getAllPermissions(@NotNull User currentUser) throws Exception {
        List<UserPermission> permissions = new ArrayList<>();
        // if user is admin-user -> can set all kind of permissions
        if (currentUser.hasUserRole(UserRoles.ROLE_ADMIN)) {
            permissions.addAll(Permissions.getPermissions(InternalPermission.class));
            permissions.addAll(Permissions.getPermissions(ClientPermission.class));
        }
        // if user is client-admin -> can set role-permissions
        if (currentUser.hasUserRole(UserRoles.ROLE_ADMIN)
                || currentUser.hasPermission(ClientPermission.IS_CLIENT_ADMIN)) {
            permissions.addAll(Permissions.getPermissions(RolePermission.class));
        }
        return permissions;
    }

}
