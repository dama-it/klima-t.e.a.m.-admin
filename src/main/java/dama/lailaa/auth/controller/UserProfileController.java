package dama.lailaa.auth.controller;

import dama.lailaa.auth.models.User;
import dama.lailaa.auth.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("api/profile")
// doesn't work with PreAuthorize?
// @BasePathAwareController
public class UserProfileController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public User getProfile(@PathVariable long id, Principal principal) throws Exception {
        return userRepository.findByUsername(principal.getName());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public User update(@PathVariable long id, @RequestBody User user, Principal principal) throws Exception {
        User currentUser = userRepository.findByUsername(principal.getName());

        currentUser.setFirstName(user.getFirstName());
        currentUser.setLastName(user.getLastName());
        currentUser.setEmail(user.getEmail());

        userRepository.save(currentUser);
        return currentUser;
    }
}
