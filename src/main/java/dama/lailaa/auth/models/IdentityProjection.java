package dama.lailaa.auth.models;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.Collection;

@Projection(name = "identityProjection", types = { User.class })
public interface IdentityProjection {
    @Value("#{target.getId()}")
    public long getId();

    @Value("#{target.getClient()?.getId()}")
    public Long getClientId();

    @Value("#{target.getClient()?.getName()}")
    public String getClientName();

    @Value("#{target.getUsername()}")
    public String getUsername();

    @Value("#{target.getUserRolesAsString()}")
    public Collection<String> getUserRoles();

    @Value("#{target.getEmail()}")
    public String getUserEmail();

    @Value("#{target.getFirstName()}")
    public String getUserFirstName();

    @Value("#{target.getLastName()}")
    public String getUserLastName();

    @Value("#{target.getFirstName()} #{target.getLastName()}")
    public String getUserFullName();

    @Value("#{target.getPermissionStrings()}")
    public Collection<String> getPermissions();

    @Value("#{target.getSelectedLanguage()}")
    public String getLocale();

}