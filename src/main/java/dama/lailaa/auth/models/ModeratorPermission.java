package dama.lailaa.auth.models;

import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class ModeratorPermission {
    @Id
    private String id;
    private String name;

    private String category;
    private String description;

    public ModeratorPermission(String id, String name, String category){
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public ModeratorPermission(String id, String name){
        this.id = id;
        this.name = name;
    }

    public ModeratorPermission(String id){
        this.id = id;
        this.name = id;
    }
}
