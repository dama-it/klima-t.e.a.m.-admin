package dama.lailaa.auth.models;

public class IdentityDto {
    private IdentityProjection identity;

    public IdentityDto() {
    }

    public IdentityProjection getIdentity() {
        return identity;
    }

    public void setIdentity(IdentityProjection identity) {
        this.identity = identity;
    }
}






