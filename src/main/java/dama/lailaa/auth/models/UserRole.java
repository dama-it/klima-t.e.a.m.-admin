package dama.lailaa.auth.models;

import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Length;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

import javax.validation.constraints.NotEmpty;
import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;


import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.*;

@Setter
@Getter
@NoArgsConstructor


@Entity
@Table(name="user_role")
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "userRoles")
    @JsonIgnore
    private Set<User> users = new HashSet<>(0);

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Length(min = 2, message = "FIELD_MUST_HAVE_AT_LEAST_2_CHARACTERS")
    private String name;

    @ElementCollection(targetClass=String.class , fetch = FetchType.EAGER)
    private Collection<String> permissions = new ArrayList<>(0);

    @Column(columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable=false, updatable=false)
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    private boolean deleted = false;

    @ManyToOne(fetch = FetchType.EAGER)
    private Client client;

    public UserRole(String name){
        this.name = name;
    }

    public boolean hasPermission(String permission) {
        for(String p : getPermissions()) {
            if(p.equals(permission)) {
                return true;
            }
        }
        return false;
    }

    public String toString(){
        return this.name.toString();
    }

    public void addPermission(String permission) {
        this.permissions.add(permission);
    }

    public void removePermission(String permission) {
        this.permissions.remove(permission);
    }

    public void update(UserRole userRole) {
        this.name = userRole.getName();
        this.client = userRole.getClient();
        this.permissions = userRole.getPermissions();
    }
}
