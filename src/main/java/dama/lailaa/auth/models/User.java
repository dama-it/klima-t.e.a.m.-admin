package dama.lailaa.auth.models;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.*;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.Pattern;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor

@Entity
// @ToString(exclude="password")
@Table(name = "user")
public class User {

    private static final PasswordEncoder pwEncoder = new BCryptPasswordEncoder();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Email(message = "EMAIL_FORMAT")
    private String email;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Length(min = 3, message = "FIELD_MUST_HAVE_AT_LEAST_3_CHARACTERS")
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Length(min = 6, message = "FIELD_MUST_HAVE_AT_LEAST_6_CHARACTERS")
    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,128}$", message = "PW_MIN_6_MAX_128_1_UPPER_1_LOWER_1_NUMERIC")
    private String password;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Length(min = 2, message = "FIELD_MUST_HAVE_AT_LEAST_2_CHARACTERS")
    private String firstName;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Length(min = 2, message = "FIELD_MUST_HAVE_AT_LEAST_2_CHARACTERS")
    private String lastName;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<UserRole> userRoles = new HashSet<>(0);

    @Transient
    private List<Long> userRoleIds = new ArrayList<Long>();

    @JsonInclude()
    @Transient
    private long userRolePk;

    private String selectedLanguage = "en";

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    private Date createdAt;

    @Version
    private Date updatedAt;

    private boolean active = true;

    @ManyToOne(fetch = FetchType.EAGER)
    private Client client;

    public User(String username, String password, String email, String firstName, String lastName,
            Set<UserRole> userRoles, Client client, boolean active) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        if (userRoles != null) {
            this.setUserRoles(userRoles);
        }
        if (client != null) {
            this.client = client;
        }
        this.active = active;
    }

    @PrePersist
    public void prePersist() {
        password = pwEncoder.encode(password);
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = new HashSet<>();
        this.userRoleIds = new ArrayList<Long>();
        for (UserRole role : userRoles) {
            if (!role.isDeleted()) {
                this.userRoles.add(role);
                this.userRoleIds.add(role.getId());
            }
        }
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public void setPasswordEncrypted(String password) {
        this.password = pwEncoder.encode(password);
    }

    /**
     * Returns all permissions of the user
     * 
     * @return
     */
    public Collection<UserPermission> getPermissions() {
        ArrayList<UserPermission> permissions = new ArrayList<>();
        Set<UserRole> userRoles = this.getUserRoles();

        if (userRoles == null) {
            return permissions;
        }
        for (UserRole role : userRoles) {
            for (String permission : role.getPermissions()) {
                permissions.add(new UserPermission(permission, permission));
            }
        }

        return permissions;
    }

    /**
     * Returns all permissions of the user as string
     * 
     * @return
     */
    public Collection<String> getPermissionStrings() {
        ArrayList<String> permissions = new ArrayList<>();
        for (UserPermission permission : this.getPermissions()) {
            permissions.add(permission.getName());
        }
        return permissions;
    }

    public boolean hasPermission(String permission) {
        Collection<UserPermission> permissions = this.getPermissions();
        for (UserPermission perm : permissions) {
            if (perm.getName().equals(permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return all role names as String array
     * 
     * @return
     */
    public Collection<String> getUserRolesAsString() {
        ArrayList<String> roleStrings = new ArrayList<>();
        Set<UserRole> userRoles = this.getUserRoles();
        if (userRoles == null) {
            return roleStrings;
        }
        for (UserRole role : userRoles) {
            roleStrings.add(role.getName());
        }
        return roleStrings;
    }

    /**
     * Checks if user has given UserRole
     * 
     * @param userRole
     * @return
     */
    public boolean hasUserRole(UserRole userRole) {
        if (this.userRoles.contains(userRole)) {
            return true;
        }
        return false;
    }

    /**
     * Checks if user has UserRole identified by the name of the role
     * 
     * @param userRoleName
     * @return
     */
    public boolean hasUserRole(String userRoleName) {
        boolean hasRole = false;
        for (UserRole role : userRoles) {
            if (role.getName().equalsIgnoreCase(userRoleName)) {
                hasRole = true;
            }
        }
        return hasRole;
    }

    public void update(User user) {
        this.email = user.getEmail();

        if (user.getPassword() != null) {
            this.password = pwEncoder.encode(user.getPassword());
        }

        if (user.getUserRoles() != null) {
            this.userRoles = user.getUserRoles();
        }

        if (user.getClient() != null) {
            this.client = user.getClient();
        }

        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();

        this.active = user.isActive();
    }

}