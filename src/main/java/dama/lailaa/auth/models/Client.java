package dama.lailaa.auth.models;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.persistence.*;


import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name="client")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "FIELD_IS_NULL")
    @NotEmpty(message = "FIELD_IS_REQUIRED")
    @Length(min = 2, message = "FIELD_MUST_HAVE_AT_LEAST_2_CHARACTERS")
    private String name;

    private boolean active = true;

    public Client(String name, boolean active){
        this.name = name;
        this.active = active;
    }


    public void update(Client client) {
        this.name = client.getName();
        this.active = client.isActive();
    }
}
