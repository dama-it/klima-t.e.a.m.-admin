package dama.lailaa.auth.models;

import javax.persistence.Id;

import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
@NoArgsConstructor

public class UserPermission {
    @Id
    private String id;
    private String name;

    private String category;
    private String description;

    public UserPermission(String id, String name, String category){
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public UserPermission(String id, String name){
        this.id = id;
        this.name = name;
    }

    public UserPermission(String id){
        this.id = id;
        this.name = id;
    }
}
