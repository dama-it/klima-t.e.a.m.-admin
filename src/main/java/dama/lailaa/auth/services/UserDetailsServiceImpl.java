package dama.lailaa.auth.services;

import static java.util.Collections.emptyList;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import dama.lailaa.auth.repositories.UserRepository;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        dama.lailaa.auth.models.User user = userRepository.findByUsername(username);
        if (user == null) {
            return null;
            //throw new UsernameNotFoundException(username);
        }
        UserRepositoryUserDetails userRepositoryUserDetails = new UserRepositoryUserDetails(user.getUsername(),
                user.getPassword(), emptyList());
        userRepositoryUserDetails.setActive(user.isActive());
        return userRepositoryUserDetails;
    }

    private final static class UserRepositoryUserDetails extends User implements UserDetails {

        private static final long serialVersionUID = 1L;
        private boolean active;

        public UserRepositoryUserDetails(String username, String password,
                Collection<? extends GrantedAuthority> authorities) {
            super(username, password, authorities);
        }

        // another methods
        public void setActive(boolean active) {
            this.active = active;
        }

        @Override
        public boolean isEnabled() {
            return this.active;
        }

        public String toString() {
            return getUsername();
        }
    }
    
}