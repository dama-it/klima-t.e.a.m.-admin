package dama.lailaa.auth.services;

import dama.lailaa.auth.models.User;
import dama.lailaa.auth.models.UserRole;
import dama.lailaa.auth.repositories.UserRepository;
import dama.lailaa.auth.repositories.UserRoleRepository;
import dama.lailaa.auth.security.UserRoles;
import dama.lailaa.auth.security.permissions.InternalPermission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Set;

@Service
public class CurrentUserServiceImpl implements CurrentUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    
    @Override
    public boolean hasPermission(Principal user, String permission) {

        if(user == null){
            return false;
        }

        User currentUser = userRepository.findByUsername(user.getName());

        boolean hasPermission = false;


        Set<UserRole> roles = currentUser.getUserRoles();

        // GLOBAL ROLES

        ArrayList<UserRole> globalRoles = userRoleRepository.findByNameContainingIgnoreCaseAndDeletedAndClient(
                UserRoles.ROLE_GLOBAL, false, null
        );

        // GLOBAL CLIENT ROLES

        if(currentUser.getClient() != null) {
            ArrayList<UserRole> globalClientRoles = userRoleRepository.findByNameContainingIgnoreCaseAndDeletedAndClient(
                    UserRoles.ROLE_CLIENT_GLOBAL, false,currentUser.getClient()
            );
            if(globalClientRoles != null && globalClientRoles.size() > 0){
                globalRoles.addAll(globalClientRoles);
            }
        }

        // ADD ROLES

        if(globalRoles.size() > 0) {
            for(UserRole r : globalRoles){
                if(!roles.contains(r)){
                    roles.add(r);
                }
            }
        }

        for(UserRole role : roles){
            if(role.getPermissions().contains(InternalPermission.IS_ADMIN) ||
                role.getPermissions().contains(permission)){
                hasPermission = true;
            }
        }

        return currentUser != null && currentUser.isActive() && hasPermission;
    }

    @Override
    public boolean hasOneOfPermissions(Principal user, String[] permissions) {

        if(user == null){
            return false;
        }

        User currentUser = userRepository.findByUsername(user.getName());

        boolean hasPermission = false;

        Set<UserRole> roles = currentUser.getUserRoles();

        // GLOBAL ROLES

        ArrayList<UserRole> globalRoles = userRoleRepository.findByNameContainingIgnoreCaseAndDeletedAndClient(
                UserRoles.ROLE_GLOBAL, false, null
        );

        // GLOBAL CLIENT ROLES

        if(currentUser.getClient() != null) {
            ArrayList<UserRole> globalClientRoles = userRoleRepository.findByNameContainingIgnoreCaseAndDeletedAndClient(
                    UserRoles.ROLE_CLIENT_GLOBAL, false,currentUser.getClient()
            );
            if(globalClientRoles != null && globalClientRoles.size() > 0){
                globalRoles.addAll(globalClientRoles);
            }
        }

        // ADD ROLES

        if(globalRoles.size() > 0) {
            for(UserRole r : globalRoles){
                if(!roles.contains(r)){
                    roles.add(r);
                }
            }
        }

        for(UserRole role : roles){
            if(role.getPermissions().contains(InternalPermission.IS_ADMIN)){
                hasPermission = true;
                break;
            }
            
            for(String permission : permissions){
                if(role.getPermissions().contains(permission)){
                    hasPermission = true;
                    break;
                }
            }
            if(hasPermission == true){
                break;
            }
        }

        return currentUser != null && currentUser.isActive() && hasPermission;
    }

    @Override
    public boolean hasPermissionOrId(Principal user, String permission, long id) {
        boolean hasPermission = this.hasPermission(user,permission);
        boolean isSameUser = this.canAccessUser(user,id);

        return hasPermission||isSameUser;
    }

    @Override
    public boolean canAccessUser(Principal user, long id) {
        if(user == null){
            return false;
        }

        User currentUser = userRepository.findByUsername(user.getName());
        return currentUser != null && currentUser.getId() == id;
    }
}
