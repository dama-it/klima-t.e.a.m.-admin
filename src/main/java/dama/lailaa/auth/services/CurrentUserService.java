package dama.lailaa.auth.services;

import java.security.Principal;

public interface CurrentUserService {
    /**
     * Determines if current user has same id as given id
     * @param user User
     * @param id long
     * @return boolean
     */
    boolean canAccessUser(Principal user, long id);

    /**
     * Determines if current user has permission to access
     * @param user User
     * @param permission String
     * @return boolean
     */
    boolean hasPermission(Principal user, String permission);

    /**
     * Determines if current user has one of given permissions
     * @param user
     * @param permissions
     * @return
     */
    boolean hasOneOfPermissions(Principal user, String[] permissions);

     /**
     * Determines if current user has permission or has same id
     * @param user User
     * @param permission String
     * @param id long
     * @return boolean
     */
    boolean hasPermissionOrId(Principal user, String permission, long id);
}
