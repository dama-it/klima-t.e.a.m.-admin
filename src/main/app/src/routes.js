import React from 'react'
import { Route } from 'react-router-dom'
import Configuration from './pages/configuration/Configuration'

export default [
  <Route exact key="configuration" path="/configuration" component={Configuration} />,
]
