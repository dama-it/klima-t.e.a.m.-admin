import enMessages from 'ra-language-english'

export default {
  ...enMessages,
  pos: {
    search: 'Search',
    profile: 'Profile',
    configuration: 'Configuration',
    language: 'Language',
    theme: {
      name: 'Theme',
      light: 'Light',
      dark: 'Dark'
    },
    dashboard: {
      welcome: {
        title: 'Lailaa',
        subtitle: 'Resistance is futile.'
      }
    }
  },
  menu: {
    userAdmin: 'User',
    users: 'Users',
  },
  resources: {
    client: {
      name: 'Client'
    },
    clients: {
      name: 'Clients'
    },
    users: {
      name: 'Users'
    },
    userRoles: {
      name: 'User Groups'
    },
    projects: {
      name: 'Projects'
    },
    panels: {
      name: 'Panels'
    },
    appUsers:{
      name: 'App Users'
    },
    addresses:{
      name: 'Addresses'
    },
    notifications:{
      name: 'Notifications'
    },
    orders:{
      name: 'Orders'
    },
    misc: {
      fields: {
        userRole: 'User Group',
        username: 'Username',
        firstName: 'Firstname',
        lastName: 'Lastname',
        email: 'Email',
        client: 'Client',
        password: 'Password',
        password2: 'Password',
        active: 'Active',
        pinned: 'Pinned',
        permissions: 'Permission',
        name: 'Name',
        search: 'Search',
        icon: 'Icon',
        banner: 'Banner',
        description: 'Description',
        locked: 'Locked',
        enabled: 'Enabled',
        content: 'Content',
        title: 'Title',
        macAddress: 'MAC',
        id: 'ID',
        birthday: 'Birthday',
        createdAt: 'Created at',
        height: 'Height',
        skintype: 'Skintype',
        weight: 'Weight',
        points: 'Points',
        active: 'Active',
        calibration: 'Calibration',
        city: 'City',
        street: 'Street',
        country: 'Country',
        zip: 'Zip',
        userId: 'User ID',
        sessionType: 'Session type',
        updatedAt: 'Updated at',
        length: 'Length',
        score: 'Score',
        header: 'Header',
        body: 'Body',
        creator: 'Creator',
        sessionUserId: 'Session User ID',
        postImage: 'Post image',
        likeId: 'Like ID',
        commentId: 'Comment ID',
        parentId: 'Parent ID',
        followId: 'Follow ID',
        type: 'Type',
        notification_read: 'Read',
        deleted: 'Deleted',
        link: 'Link',
        videoId: 'Video ID',
        membership: 'Membership'
      },
      userRoleCategories: {
        cat_ADMINISTRATION: 'Administration',
        cat_OTHER: 'Other'
      }
    }
  }, 
  ROLE_ADMIN: 'Administrator',
  ROLE_CLIENT_ADMIN: 'Mandanten Administrator',
  IS_ADMIN: 'Is Administrator',
  ERROR: {
    ERROR: 'System error.',
  }
}
