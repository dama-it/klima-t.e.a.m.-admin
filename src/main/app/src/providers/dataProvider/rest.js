import springJsonServer from "./springJsonServer";

const restClient = springJsonServer("/api");
export default (type, resource, params) =>
  new Promise(
    resolve =>
      setTimeout(() => resolve(restClient(type, resource, params)), 500),
    reject => console.log("REJECT", reject)
  );
