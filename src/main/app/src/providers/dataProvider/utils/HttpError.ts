class HttpError extends Error {

    constructor(
        public readonly message:any,
        public readonly status:any,
        public readonly body = null
    ) {
        super(message);
        this.name = this.constructor.name;
        if(false)  {
//      if (Error.captureStackTrace && typeof Error.captureStackTrace === 'function') {
//            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = new Error(message).stack;
        }
        this.stack = new Error().stack;
    }
}

export default HttpError;