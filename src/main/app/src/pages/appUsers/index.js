import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  DateField,
  DateTimeInput,
  ReferenceInput,
  SelectInput,
  translate,
  FunctionField,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const AppUserFilter = props => (
  <Filter {...props}>
    <TextInput
      elStyle={{ width: 380 }}
      label="resources.misc.fields.id"
      source="id"
    />
    <TextInput
      elStyle={{ width: 380 }}
      label="resources.misc.fields.username"
      source="username"
    />
    <TextInput label="resources.misc.fields.firstName" source="firstName" />
    <TextInput label="resources.misc.fields.lastName" source="lastName" />
    <TextField label="resources.misc.fields.membership" source="membership" />
    <BooleanInput
      label="resources.misc.fields.active"
      source="active"
      options={{ defaultToggled: true }}
    />
    <BooleanInput
      label="resources.misc.fields.calibration"
      source="calibration"
      options={{ defaultToggled: true }}
    />
  </Filter>
);

export const AppUserList = props => (
  <List {...props} filters={<AppUserFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.username" source="username" />
      <TextField label="resources.misc.fields.firstName" source="firstName" />
      <TextField label="resources.misc.fields.lastName" source="lastName" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <TextField label="resources.misc.fields.points" source="points" />
      <TextField label="resources.misc.fields.membership" source="membership.name"/>
      <BooleanField label="resources.misc.fields.active" source="active" />
      <BooleanField label="resources.misc.fields.calibration" source="calibration" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);

const AppUserTitle = ({ record }) => {
    return (
      <span>App user {record ? `"${record.username}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const AppUserShow = props => (
  <Show title={<AppUserTitle />} actions={<Actions />} {...props}>
    <SimpleShowLayout>
      <TextField label="resources.misc.fields.id" source="id" />
      <BooleanField label="resources.misc.fields.active" source="active" />
      <BooleanField label="resources.misc.fields.calibration" source="calibration" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <TextField label="resources.misc.fields.email" source="email" />
      <TextField label="resources.misc.fields.firstName" source="firstName" />
      <TextField label="resources.misc.fields.lastName" source="lastName" />
      <TextField label="resources.misc.fields.points" source="points" />
      <TextField label="resources.misc.fields.username" source="username" />
    </SimpleShowLayout>
  </Show>
);

export const AppUserEdit = ({ permissions, ...props }) => (
  <Edit title={<AppUserTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
      <TextInput
        disabled
        label="resources.misc.fields.username"
        source="username"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.password"
        source="password"
        type="password"
        validate={[passwordEdit()]}
      />
      <TextInput
        label="resources.misc.fields.password2"
        source="password2"
        type="password"
        validate={[passwordEquals()]}
      />
      <TextInput
        disabled
        label="resources.misc.fields.id"
        source="id"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.email"
        source="email"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.firstName"
        source="firstName"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.lastName"
        source="lastName"
        validate={[required()]}
      />
      <BooleanInput
        label="resources.misc.fields.active"
        source="active"
        options={{ defaultToggled: true }}
      />
      <BooleanInput
        label="resources.misc.fields.calibration"
        source="calibration"
        options={{ defaultToggled: true }}
      />
    </SimpleForm>
  </Edit>
);

export const AppUserCreate = ({ permissions, ...props }) => (
  <Create title="Create a App user" {...props}>
    <SimpleForm redirect="list">
      <TextInput
        label="resources.misc.fields.email"
        source="email"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.firstName"
        source="firstName"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.lastName"
        source="lastName"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.points"
        source="points"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.username"
        source="username"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.password"
        source="password"
        type="password"
        validate={[required(), password()]}
      />
      <TextInput
        label="resources.misc.fields.password2"
        source="password2"
        type="password"
        validate={[required(), passwordEquals()]}
      />
      
      <BooleanInput
        label="resources.misc.fields.active"
        source="active"
        options={{ defaultToggled: true }}
      />
      <BooleanInput
        label="resources.misc.fields.calibration"
        source="calibration"
        options={{ defaultToggled: true }}
      />
    </SimpleForm>
  </Create>
);