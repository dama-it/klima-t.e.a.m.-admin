import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  ReferenceInput,
  SelectInput,
  translate,
  FunctionField,
  DateTimeInput,
  DateField,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const SettingFilter = props => (
  <Filter {...props}>
    <TextInput label="resources.misc.fields.id" source="id" />
  </Filter>
);

export const SettingList = props => (
  <List {...props} filters={<SettingFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <BooleanField label="resources.misc.fields.s1" source="s1" />
      <BooleanField label="resources.misc.fields.s2" source="s2" />
      <BooleanField label="resources.misc.fields.s3" source="s3" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);

const SettingTitle = ({ record }) => {
    return (
      <span>Setting {record ? `"${record.id}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const SettingShow = props => (
  <Show title={<SettingTitle />} actions={<Actions />} {...props}>
    <SimpleShowLayout>
      <TextField label="resources.misc.fields.id" source="id" />
      <BooleanField label="resources.misc.fields.s1" source="s1" />
      <BooleanField label="resources.misc.fields.s2" source="s2" />
      <BooleanField label="resources.misc.fields.s3" source="s3" />
    </SimpleShowLayout>
  </Show>
);

export const SettingEdit = ({ permissions, ...props }) => (
  <Edit title={<SettingTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
      <TextInput
        disabled
        label="resources.misc.fields.id"
        source="id"
      />
      <BooleanInput
        label="resources.misc.fields.s1"
        source="s1"
      />
      <BooleanInput
        label="resources.misc.fields.s2"
        source="s2"
      />
      <BooleanInput
        label="resources.misc.fields.s3"
        source="s3"
      />
    </SimpleForm>
  </Edit>
);

export const SettingCreate = ({ permissions, ...props }) => (
  <Create title="Create a Setting" {...props}>
    <SimpleForm redirect="list">
      <BooleanInput
        label="resources.misc.fields.s1"
        source="s1"
      />
      <BooleanInput
        label="resources.misc.fields.s2"
        source="s2"
      />
      <BooleanInput
        label="resources.misc.fields.s3"
        source="s3"
      />
    </SimpleForm>
  </Create>
);