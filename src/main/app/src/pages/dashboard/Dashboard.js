// React imports
import React, { Component, Fragment } from "react";

// React Admin imports
import { Responsive, Title } from "react-admin";

// Material imports
import Grid from "@material-ui/core/Grid";

// Other imports
import Welcome from "./Welcome";

const styles = {
  flex: { display: "flex" },
  flexColumn: { display: "flex", flexDirection: "column" },
  leftCol: { flex: 1, marginRight: "1em" },
  rightCol: { flex: 1, marginLeft: "1em" },
  singleCol: { marginBottom: "30px" },

  root: {
    flexGrow: 1
  },
  paper: {
    padding: "15px",
    textAlign: "center"
  }
};

class Dashboard extends Component {
  componentDidMount() {
    /* const aMonthAgo = new Date();
        aMonthAgo.setDate(aMonthAgo.getDate() - 30);

        dataProviderFactory("rest").then(
            dataProvider => {

            }
        );*/
  }

  render() {
    return (
      <Fragment>
        <Title title="Lailaa Admin" />
        <Responsive
          xsmall={
            <div>
              <div style={styles.flexColumn}>
                <div style={{ marginBottom: "2em" }}>
                  <Welcome />
                </div>
              </div>
            </div>
          }
          small={
            <div style={styles.flex}>
              <div style={styles.leftCol}>
                <div style={styles.singleCol}>
                  <Welcome />
                </div>
              </div>
            </div>
          }
          medium={
            <div style={styles.root}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Welcome />
                </Grid>
              </Grid>
            </div>
          }
        />
      </Fragment>
    );
  }
}

export default Dashboard;
