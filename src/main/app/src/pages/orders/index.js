import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  DateField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  ReferenceInput,
  SelectInput,
  DateTimeInput,
  translate,
  FunctionField,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const OrderFilter = props => (
  <Filter {...props}>
    <TextInput
      elStyle={{ width: 380 }}
      label="resources.misc.fields.id"
      source="id"
    />
    <TextInput
      elStyle={{ width: 380 }}
      label="resources.misc.fields.type"
      source="type"
    />
    <TextInput
      elStyle={{ width: 380 }}
      label="resources.misc.fields.customer"
      source="customer"
    />
  </Filter>
);

export const OrderList = props => (
  <List {...props} filters={<OrderFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.type" source="type" />
      <TextField label="resources.misc.fields.customer" source="customer" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);

const OrderTitle = ({ record }) => {
    return (
      <span>{record ? `"${record.name}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const OrderShow = props => (
  <Show 
  title={<OrderTitle />} 
  actions={<Actions />}
  {...props}>
    <SimpleShowLayout>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.type" source="type" />
      <TextField label="resources.misc.fields.customer" source="customer" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
    </SimpleShowLayout>
  </Show>
);

export const OrderEdit = ({ permissions, ...props }) => (
  <Edit title={<OrderTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
      <TextInput
        disabled
        label="resources.misc.fields.id"
        source="id"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.type"
        source="type"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.customer"
        source="customer"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.createdAt"
        source="createdAt"
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
);

export const OrderCreate = ({ permissions, ...props }) => (
  <Create title="Create a Order" {...props}>
    <SimpleForm redirect="list">
      <TextInput
        label="resources.misc.fields.type"
        source="type"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.customer"
        source="customer"
        validate={[required()]}
      />
    </SimpleForm>
  </Create>
);