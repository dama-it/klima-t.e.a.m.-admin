import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  ReferenceInput,
  SelectInput,
  translate,
  FunctionField,
  DateTimeInput,
  DateField,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const NotificationFilter = props => (
  <Filter {...props}>
    <TextInput label="resources.misc.fields.id" source="id" />
    <TextInput label="resources.misc.fields.type" source="type" />
    <TextInput label="resources.misc.fields.body" source="body" />
    <BooleanInput
      label="resources.misc.fields.notification_read"
      source="notification_read"
      options={{ defaultToggled: true }}
    />
    <BooleanInput
      label="resources.misc.fields.deleted"
      source="deleted"
      options={{ defaultToggled: true }}
    />
  </Filter>
);

export const NotificationList = props => (
  <List {...props} filters={<NotificationFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <DateField label="resources.misc.fields.updatedAt" source="updatedAt" />
      <TextField label="resources.misc.fields.type" source="type" />
      <TextField label="resources.misc.fields.body" source="body" />
      <BooleanField label="resources.misc.fields.notification_read" source="notification_read" />
      <BooleanField label="resources.misc.fields.deleted" source="deleted" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);

const NotificationTitle = ({ record }) => {
    return (
      <span>Notification {record ? `"${record.id}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const NotificationShow = props => (
  <Show title={<NotificationTitle />} actions={<Actions />} {...props}>
    <SimpleShowLayout>
      <TextField label="resources.misc.fields.id" source="id" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <DateField label="resources.misc.fields.updatedAt" source="updatedAt" />
      <TextField label="resources.misc.fields.userId" source="userId" />
      <TextField label="resources.misc.fields.type" source="type" />
      <TextField label="resources.misc.fields.typeId" source="typeId" />
      <TextField label="resources.misc.fields.body" source="body" />
      <BooleanField label="resources.misc.fields.notification_read" source="notification_read" />
      <BooleanField label="resources.misc.fields.deleted" source="deleted" />
    </SimpleShowLayout>
  </Show>
);

export const NotificationEdit = ({ permissions, ...props }) => (
  <Edit title={<NotificationTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
      <TextInput
        disabled
        label="resources.misc.fields.id"
        source="id"
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.createdAt"
        source="createdAt"
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.updatedAt"
        source="updatedAt"
      />
      <TextInput
        label="resources.misc.fields.type"
        source="type"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.body"
        source="body"
        validate={[required()]}
      />
      <BooleanInput
        label="resources.misc.fields.notification_read"
        source="notification_read"
      />
      <BooleanInput
        label="resources.misc.fields.deleted"
        source="deleted"
      />
    </SimpleForm>
  </Edit>
);

export const NotificationCreate = ({ permissions, ...props }) => (
  <Create title="Create a Notification" {...props}>
    <SimpleForm redirect="list">
      <TextInput
        label="resources.misc.fields.type"
        source="type"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.body"
        source="body"
        validate={[required()]}
      />
      <BooleanInput
        label="resources.misc.fields.notification_read"
        source="notification_read"
      />
      <BooleanInput
        label="resources.misc.fields.deleted"
        source="deleted"
      />
    </SimpleForm>
  </Create>
);