import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  Filter,
  AutocompleteInput,
  TabbedForm,
  FormTab,
  CardActions,
  CreateButton,
  RefreshButton,
  TopToolbar
} from "react-admin";
import Icon from "@material-ui/icons/LocalTaxi";


export const ClientIcon = Icon;

const ProjectTitle = ({ record }) => {
  return <span>Project {record ? `"${record.name}"` : ""}</span>;
};




export const ProjectList = props => (
  <List {...props} >
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
    </Datagrid>
  </List>
);


export const ProjectCreate = props => (
  <Create title="Create a Project" {...props} >
    <SimpleForm redirect="list">
        <TextInput source="name" />
        <TextInput source="Author" />
        <AutocompleteInput source="designMethod" choices={[
            { id: 'EC', name: 'Eurocode ' },
            { id: 'EC_DE', name: 'DIN EN' },
            { id: 'EC_CH', name: 'SIA / SN EN' },
            { id: 'EC_UK', name: 'BS EN' },
            { id: 'EC_FR', name: 'NF EN' },
            { id: 'EC_IT', name: 'UNI EN' },
            { id: 'EC_NL', name: 'NEN EN' },
            { id: 'EC_BE', name: 'NBN EN' },
            { id: 'EC_AT', name: 'ÖNORM EN' },
            { id: 'EC_PL', name: 'PN EN' },
            { id: 'EC_SE', name: 'SS EN' },
            { id: 'EC_SE', name: 'DS EN' },
            { id: 'EC_ES', name: 'UNE EN' },
            { id: 'EC_PT', name: 'NP EN' },
        ]} />
        <TextInput source="Address" />
        <TextInput source="Customer" />
        <TextInput source="phone" />
        <TextInput source="email" />
    </SimpleForm>
  </Create>
);
