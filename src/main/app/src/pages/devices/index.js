import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  DateField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  ReferenceInput,
  SelectInput,
  DateTimeInput,
  translate,
  FunctionField,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const DeviceFilter = props => (
  <Filter {...props}>
    <TextInput
      elStyle={{ width: 380 }}
      label="resources.misc.fields.id"
      source="id"
    />
    <TextInput
      elStyle={{ width: 380 }}
      label="resources.misc.fields.name"
      source="name"
    />
    <TextInput
      elStyle={{ width: 380 }}
      label="resources.misc.fields.macAddress"
      source="macAddress"
    />
  </Filter>
);

export const DeviceList = props => (
  <List {...props} filters={<DeviceFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.name" source="name" />
      <TextField label="resources.misc.fields.macAddress" source="macAddress" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);

const DeviceTitle = ({ record }) => {
    return (
      <span>Device {record ? `"${record.name}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const DeviceShow = props => (
  <Show title={<DeviceTitle />} actions={<Actions />} {...props}>
    <SimpleShowLayout>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.name" source="name" />
      <TextField label="resources.misc.fields.macAddress" source="macAddress" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
    </SimpleShowLayout>
  </Show>
);

export const DeviceEdit = ({ permissions, ...props }) => (
  <Edit title={<DeviceTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
      <TextInput
        disabled
        label="resources.misc.fields.id"
        source="id"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.name"
        source="name"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.macAddress"
        source="macAddress"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.createdAt"
        source="createdAt"
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
);

export const DeviceCreate = ({ permissions, ...props }) => (
  <Create title="Create a Device" {...props}>
    <SimpleForm redirect="list">
      <TextInput
        label="resources.misc.fields.name"
        source="name"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.macAddress"
        source="macAddress"
        validate={[required()]}
      />
    </SimpleForm>
  </Create>
);