import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  Filter,
  AutocompleteInput,
  TabbedForm,
  FormTab,
  CardActions,
  CreateButton,
  RefreshButton,
  TopToolbar
} from "react-admin";
import Icon from "@material-ui/icons/LocalTaxi";


export const ClientIcon = Icon;

const ProjectTitle = ({ record }) => {
  return <span>Project {record ? `"${record.name}"` : ""}</span>;
};


export const PanelList = props => (
  <List {...props} >
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="power" />
      <TextField source="width" />
      <TextField source="length" />
      <TextField source="height" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);


export const PanelCreate = props => (
  <Create title="Create a Project" {...props} >
    <SimpleForm redirect="list">
        <TextInput source="name" />
        <TextInput source="width" />
        <TextInput source="length" />
        <TextInput source="height" />
        <TextInput source="power" />
    </SimpleForm>
  </Create>
);



export const PanelEdit = props => (
  <Edit title="Edit a Project" {...props} >
    <SimpleForm redirect="list">
        <TextInput source="name" />
        <TextInput source="width" />
        <TextInput source="length" />
        <TextInput source="height" />
        <TextInput source="power" />
    </SimpleForm>
  </Edit>
);

