import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  ReferenceInput,
  SelectInput,
  translate,
  FunctionField,
  DateTimeInput,
  DateField,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const SessionTypeFilter = props => (
  <Filter {...props}>
    <TextInput label="resources.misc.fields.id" source="id" />
    <TextInput label="resources.misc.fields.name" source="name" />
    <TextInput label="resources.misc.fields.createdAt" source="createdAt" />
    <TextInput label="resources.misc.fields.length" source="length" />
  </Filter>
);

export const SessionTypeList = props => (
  <List {...props} filters={<SessionTypeFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.name" source="name" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <DateField label="resources.misc.fields.updatedAt" source="updatedAt" />
      <TextField label="resources.misc.fields.length" source="length" />
      <EditButton label="" />
    </Datagrid>
  </List>
);

const SessionTypeTitle = ({ record }) => {
    return (
      <span>Session type {record ? `"${record.name}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const SessionTypeShow = props => (
  <Show title={<SessionTypeTitle />} actions={<Actions />} {...props}>
    <SimpleShowLayout>
    <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.name" source="name" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <DateField label="resources.misc.fields.updatedAt" source="updatedAt" />
      <TextField label="resources.misc.fields.length" source="length" />
    </SimpleShowLayout>
  </Show>
);

export const SessionTypeEdit = ({ permissions, ...props }) => (
  <Edit title={<SessionTypeTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
      <TextInput
        label="resources.misc.fields.name"
        source="name"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.createdAt"
        source="createdAt"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.updatedAt"
        source="updatedAt"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.length"
        source="length"
      />
    </SimpleForm>
  </Edit>
);

export const SessionTypeCreate = ({ permissions, ...props }) => (
  <Create title="Create a Session type" {...props}>
    <SimpleForm redirect="list">
      <TextInput
        label="resources.misc.fields.name"
        source="name"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.length"
        source="length"
        validate={[required()]}
      />
    </SimpleForm>
  </Create>
);