import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  ReferenceInput,
  SelectInput,
  translate,
  FunctionField,
  DateField,
  DateTimeInput,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const AddressFilter = props => (
  <Filter {...props}>
    <TextInput label="resources.misc.fields.id" source="id" />
    <TextInput label="resources.misc.fields.city" source="city" />
    <TextInput label="resources.misc.fields.street" source="street" />
    <TextInput label="resources.misc.fields.country" source="country" />
    <TextInput label="resources.misc.fields.zip" source="zip" />
    <TextInput label="resources.misc.fields.createdAt" source="createdAt" />
  </Filter>
);

export const AddressList = props => (
  <List {...props} filters={<AddressFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.city" source="city" />
      <TextField label="resources.misc.fields.street" source="street" />
      <TextField label="resources.misc.fields.country" source="country" />
      <TextField label="resources.misc.fields.zip" source="zip" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);

const AddressTitle = ({ record }) => {
    return (
      <span>Address {record ? `"${record.name}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const AddressShow = props => (
  <Show title={<AddressTitle />} actions={<Actions />} {...props}>
    <SimpleShowLayout>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.city" source="city" />
      <TextField label="resources.misc.fields.street" source="street" />
      <TextField label="resources.misc.fields.country" source="country" />
      <TextField label="resources.misc.fields.zip" source="zip" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
    </SimpleShowLayout>
  </Show>
);

export const AddressEdit = ({ permissions, ...props }) => (
  <Edit title={<AddressTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
      <TextInput
        disabled
        label="resources.misc.fields.id"
        source="id"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.city"
        source="city"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.street"
        source="street"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.country"
        source="country"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.zip"
        source="zip"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.createdAt"
        source="createdAt"
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
);

export const AddressCreate = ({ permissions, ...props }) => (
  <Create title="Create an Address" {...props}>
    <SimpleForm redirect="list">
      <TextInput
        label="resources.misc.fields.city"
        source="city"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.street"
        source="street"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.country"
        source="country"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.zip"
        source="zip"
        validate={[required()]}
      />
    </SimpleForm>
  </Create>
);