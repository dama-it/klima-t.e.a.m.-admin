import React from "react";
import { Edit, SimpleForm, TextInput } from "react-admin";

const redirect = (basePath, id, data) => `/profile/1`;

export const ProfileEdit = props => (
  <Edit {...props}>
    <SimpleForm redirect={redirect}>
      <TextInput source="firstName" />
      <TextInput source="lastName" />
      <TextInput source="email" />
    </SimpleForm>
  </Edit>
);
