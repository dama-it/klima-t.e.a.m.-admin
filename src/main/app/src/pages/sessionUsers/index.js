import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  ReferenceInput,
  SelectInput,
  translate,
  FunctionField,
  DateField,
  DateTimeInput,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const SessionUserFilter = props => (
  <Filter {...props}>
    <TextInput label="resources.misc.fields.id" source="id" />
    <TextInput label="resources.misc.fields.sessionType" source="sessionType" />
    <TextInput label="resources.misc.fields.length" source="length" />
    <TextInput label="resources.misc.fields.score" source="score" />
    <TextInput label="resources.misc.fields.createdAt" source="createdAt" />
  </Filter>
);

export const SessionUserList = props => (
  <List {...props} filters={<SessionUserFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.sessionType" source="sessionType" />
      <TextField label="resources.misc.fields.length" source="length" />
      <TextField label="resources.misc.fields.score" source="score" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <DateField label="resources.misc.fields.updatedAt" source="updatedAt" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);

const SessionUserTitle = ({ record }) => {
    return (
      <span>Session user {record ? `"${record.name}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const SessionUserShow = props => (
  <Show title={<SessionUserTitle />} actions={<Actions />} {...props}>
    <SimpleShowLayout>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.userId" source="userId" />
      <TextField label="resources.misc.fields.sessionType" source="sessionType" />
      <TextField label="resources.misc.fields.length" source="length" />
      <TextField label="resources.misc.fields.score" source="score" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <DateField label="resources.misc.fields.updatedAt" source="updatedAt" />
    </SimpleShowLayout>
  </Show>
);

export const SessionUserEdit = ({ permissions, ...props }) => (
  <Edit title={<SessionUserTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
    <TextInput
        disabled
        label="resources.misc.fields.id"
        source="id"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.createdAt"
        source="createdAt"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.updatedAt"
        source="updatedAt"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.sessionType"
        source="sessionType"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.length"
        source="length"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.score"
        source="score"
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
);

export const SessionUserCreate = ({ permissions, ...props }) => (
  <Create title="Create a Session User" {...props}>
    <SimpleForm redirect="list">
      <TextInput
        label="resources.misc.fields.sessionType"
        source="sessionType"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.length"
        source="length"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.score"
        source="score"
        validate={[required()]}
      />
    </SimpleForm>
  </Create>
);