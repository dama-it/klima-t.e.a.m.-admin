import React from "react";
import {
  List,
  Datagrid,
  Edit,
  Create,
  SimpleForm,
  TextField,
  EditButton,
  TextInput,
  BooleanInput,
  BooleanField,
  DateTimeInput,
  DateField,
  ReferenceInput,
  SelectInput,
  translate,
  FunctionField,
  Show,
  SimpleShowLayout,
  CardActions,
  ListButton,
  Filter
} from "react-admin";
import {
    required,
    password,
    passwordEdit,
    passwordEquals
} from "../../validators/validators";

const PostFilter = props => (
  <Filter {...props}>
    <TextInput label="resources.misc.fields.id" source="id" />
    <TextInput label="resources.misc.fields.header" source="header" />
    <TextInput label="resources.misc.fields.body" source="body" />
    <TextInput label="resources.misc.fields.creator" source="header" />
  </Filter>
);

export const PostList = props => (
  <List {...props} filters={<PostFilter />}>
    <Datagrid>
      <TextField label="resources.misc.fields.id" source="id" />
      <TextField label="resources.misc.fields.header" source="header" />
      <TextField label="resources.misc.fields.body" source="body" />
      <DateField label="resources.misc.fields.createdAt" source="createdAt" />
      <DateField label="resources.misc.fields.updatedAt" source="updatedAt" />
      <TextField label="resources.misc.fields.creator" source="creator" />
      <TextField label="resources.misc.fields.postImage" source="postImage" />
      <EditButton label="" /> 
    </Datagrid>
  </List>
);

const PostTitle = ({ record }) => {
    return (
      <span>Follow {record ? `"${record.name}"` : ""}</span>
    );
};

const cardActionStyle = {
    zIndex: 2,
    display: "inline-block",
    float: "right"
};

const Actions = ({ basePath, data, refresh }) => (
  <CardActions style={cardActionStyle}>
    <EditButton basePath={basePath} record={data} />
    <ListButton basePath={basePath} />
  </CardActions>
);

export const PostShow = props => (
  <Show title={<PostTitle />} actions={<Actions />} {...props}>
    <SimpleShowLayout>
        <TextField label="resources.misc.fields.id" source="id" />
        <TextField label="resources.misc.fields.header" source="header" />
        <TextField label="resources.misc.fields.body" source="body" />
        <DateField label="resources.misc.fields.createdAt" source="createdAt" />
        <DateField label="resources.misc.fields.updatedAt" source="updatedAt" />
        <TextField label="resources.misc.fields.creator" source="creator" />
        <TextField label="resources.misc.fields.postImage" source="postImage" />
    </SimpleShowLayout>
  </Show>
);

export const PostEdit = ({ permissions, ...props }) => (
  <Edit title={<PostTitle />} actions={<CardActions />} {...props}>
    <SimpleForm>
      <TextInput
        disabled
        label="resources.misc.fields.id"
        source="id"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.header"
        source="header"
        validate={[required()]}
      />
      <TextInput
        multiline 
        label="resources.misc.fields.body"
        source="body"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.createdAt"
        source="createdAt"
        validate={[required()]}
      />
      <DateTimeInput
        disabled
        label="resources.misc.fields.updatedAt"
        source="updatedAt"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.postImage"
        source="postImage"
      />
    </SimpleForm>
  </Edit>
);

export const PostCreate = ({ permissions, ...props }) => (
  <Create title="Create a Follow" {...props}>
    <SimpleForm redirect="list">
      <TextInput
        label="resources.misc.fields.header"
        source="header"
        validate={[required()]}
      />
      <TextInput
        multiline 
        label="resources.misc.fields.body"
        source="body"
        validate={[required()]}
      />
      <TextInput
        label="resources.misc.fields.postImage"
        source="postImage"
        validate={[required()]}
      />
    </SimpleForm>
  </Create>
);