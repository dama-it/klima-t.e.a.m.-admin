import "babel-polyfill";
import React, { Component } from "react";
import { Admin, Resource } from "react-admin";
import "./theme/styles/App.css";
import authProvider from "./providers/authProvider";
import Login from "./Login";
import Layout from "./theme/layouts/Layout";
import Menu from "./theme/partials/Menu.js";
import { Dashboard } from "./pages/dashboard";
import customRoutes from "./routes";
import enMessages from "./i18n/en";
import dataProviderFactory from "./providers/dataProvider";
import addUploadFeature from "./providers/dataProvider/addUploadFeature";
import { ProfileEdit } from "./pages/profile";
import userReducer from "./reducer";
import userSaga from "./sagas/";
import { ClientList, ClientCreate, ClientEdit } from "./pages/client";
import { UserList, UserCreate, UserEdit, UserShow } from "./pages/user";
import { PanelList,PanelCreate, PanelEdit} from "./pages/panels";
import { DeviceList, DeviceCreate, DeviceEdit, DeviceShow } from "./pages/devices";
import { UserRoleList, UserRoleCreate, UserRoleEdit, UserRoleShow } from "./pages/userRoles";
import { AppUserList, AppUserCreate, AppUserEdit, AppUserShow } from "./pages/appUsers";
import { AddressList, AddressCreate, AddressEdit, AddressShow } from "./pages/addresses";
import { SessionUserList, SessionUserCreate, SessionUserEdit, SessionUserShow } from "./pages/sessionUsers";
import { SessionTypeList, SessionTypeCreate, SessionTypeEdit, SessionTypeShow } from "./pages/sessionTypes";
import { NotificationList, NotificationCreate, NotificationEdit, NotificationShow } from "./pages/notifications";
import { SettingList, SettingCreate, SettingEdit, SettingShow } from "./pages/settings";
import { OrderCreate, OrderEdit, OrderList, OrderShow } from "./pages/orders";


const i18nProvider = () => {
  // Always fallback on english
  return enMessages;
};

class App extends Component {
  state = { uploadCapableDataProvider: null };

  async componentWillMount() {
    const dataProvider = await dataProviderFactory("rest");

    const uploadCapableDataProvider = addUploadFeature(dataProvider);

    this.setState({ uploadCapableDataProvider });
  }

  componentWillUnmount() {
    this.dataProvider();
  }

  render() {
    const { uploadCapableDataProvider } = this.state;

    if (!uploadCapableDataProvider) {
      return (
        <div className="loader-container">
          <div className="loader">Loading...</div>
        </div>
      );
    }

    return (
      <Admin
        title="Lailaa Admin"
        dataProvider={uploadCapableDataProvider}
        authProvider={authProvider}
        customRoutes={customRoutes}
        customReducers={{ identity: userReducer }}
        customSagas={[userSaga]}
        loginPage={Login}
        appLayout={Layout}
        menu={Menu}
        locale="en"
        i18nProvider={i18nProvider}
      >
        {permissions => [
          <Resource name="profile" edit={ProfileEdit} />,

          <Resource
            name="clients"
            list={ClientList}
            edit={ClientEdit}
            create={ClientCreate}
          />,

          <Resource
            name="users"
            list={UserList}
            edit={UserEdit}
            create={UserCreate}
            show={UserShow}
          />,

          <Resource
            name="userRoles"
            list={UserRoleList}
            edit={UserRoleEdit}
            create={UserRoleCreate}
            show={UserRoleShow}
          />,

          <Resource
            name="panels"
            list={PanelList}
            edit={PanelEdit}
            create={PanelCreate}
          />,

          <Resource
            name="devices"
            list={DeviceList}
            edit={DeviceEdit}
            create={DeviceCreate}
            show={DeviceShow}
          />,

          <Resource
            name="appUsers"
            list={AppUserList}
            edit={AppUserEdit}
            create={AppUserCreate}
            show={AppUserShow}
          />,

          <Resource
            name="addresses"
            list={AddressList}
            edit={AddressEdit}
            create={AddressCreate}
            show={AddressShow}
          />,

          <Resource
            name="sessionUsers"
            list={SessionUserList}
            edit={SessionUserEdit}
            create={SessionUserCreate}
            show={SessionUserShow}
          />,

          <Resource
            name="sessionTypes"
            list={SessionTypeList}
            edit={SessionTypeEdit}
            create={SessionTypeCreate}
            show={SessionTypeShow}
          />,

          <Resource
            name="notifications"
            list={NotificationList}
            edit={NotificationEdit}
            create={NotificationCreate}
            show={NotificationShow}
          />,

          <Resource
            name="settings"
            list={SettingList}
            edit={SettingEdit}
            create={SettingCreate}
            show={SettingShow}
          />,
          <Resource
            name="orders"
            list={OrderList}
            edit={OrderEdit}
            create={OrderCreate}
            show={OrderShow}
          />,
        ]}
      </Admin>
    );
  }
}

export default App;
