import React, { Component } from "react";
import { FileInput } from "react-admin";
import restClient from "../providers/dataProvider/rest";
import FileFieldInput from "./FileFieldInput";
import FilePreview from "./FilePreview";
import { touch, untouch, change } from "redux-form";
import { REDUX_FORM_NAME } from "react-admin";
import { connect } from "react-redux";

class FileUploadInput extends Component {
  uploadFileSuccess = (data, rawFile) => {
    if (data && data) {
      data = data.data;
    }
    rawFile.documentId = data.id;
    rawFile.errorMsg = null;
    const { dispatch } = this.props;
    setTimeout(() => {
      dispatch(change(REDUX_FORM_NAME, "tmp", new Date().getTime()));
    }, 10);
  };

  uploadFileError = (e, rawFile) => {
    rawFile.documentId = null;
    rawFile.errorMsg = e.message;
    const { dispatch } = this.props;
    setTimeout(() => {
      dispatch(touch(REDUX_FORM_NAME, "documents[0].name"));
      dispatch(untouch(REDUX_FORM_NAME, "documents[0].name"));
    }, 10);
  };

  uploadFile(file) {
    file = file[0];
    let formData = new FormData();
    formData.append("file", file);
    formData.append("name", file.name);
    restClient("UPLOAD", "upload", formData).then(
      data => this.uploadFileSuccess(data, file),
      e => this.uploadFileError(e, file)
    );
  }

  render() {
    const prev = this.props.previewSource + ".id";

    return (
      <span>
        <FileInput
          source={this.props.source}
          label={this.props.label}
          multiple={false}
          options={{ onDropAccepted: file => this.uploadFile(file) }}
        >
          <FileFieldInput src="id" title="fileName" source="id" />
        </FileInput>
        <FilePreview source={prev} validate={[]}></FilePreview>
      </span>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(FileUploadInput); //eslint-disable-line react-redux/prefer-separate-component-file
