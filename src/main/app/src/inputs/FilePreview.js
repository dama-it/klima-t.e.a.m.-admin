// React imports
import React, { Component } from "react";

// React Admin imports
import { FormDataConsumer } from "react-admin";

// Other imports
import PdfPreview from "./PdfPreview";
import ImagePreview from "./ImagePreview";
import {
  getId,
  getNewFileId,
  getFileType,
  getNewFileType
} from "./FileUploadHelper";

class FilePreview extends Component {
  render = () => {
    return (
      <FormDataConsumer>
        {({ formData, ...rest }) => {
          return (
            <div>
              {formData &&
                getNewFileId(formData, this.props.source) === null &&
                getFileType(formData, this.props.source).indexOf("image") >=
                  0 && (
                  <ImagePreview imageId={getId(formData, this.props.source)} />
                )}
              {formData &&
                getNewFileId(formData, this.props.source) !== null &&
                getNewFileType(formData, this.props.source).indexOf("image") >=
                  0 && (
                  <ImagePreview
                    imageId={getNewFileId(formData, this.props.source)}
                  />
                )}
              {formData &&
                getNewFileId(formData, this.props.source) === null &&
                getFileType(formData, this.props.source) ===
                  "application/pdf" && (
                  <PdfPreview pdfid={getId(formData, this.props.source)} />
                )}
              {formData &&
                getNewFileId(formData, this.props.source) !== null &&
                getNewFileType(formData, this.props.source) ===
                  "application/pdf" && (
                  <PdfPreview
                    pdfid={getNewFileId(formData, this.props.source)}
                  />
                )}
            </div>
          );
        }}
      </FormDataConsumer>
    );
  };
}

export default FilePreview;
