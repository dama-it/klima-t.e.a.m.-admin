// React imports
import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import compose from "recompose/compose";
import {
  translate,
  DashboardMenuItem,
  MenuItemLink,
  Responsive,
  WithPermissions
} from "react-admin";
import { withStyles } from "@material-ui/core/styles";
import Group from "@material-ui/icons/Group";
import MenuElem from "./MenuElem";
import AppsIcon from '@material-ui/icons/Apps';
import PersonIcon from '@material-ui/icons/Person';
import HomeIcon from '@material-ui/icons/Home';
import DevicesIcon from '@material-ui/icons/Devices';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import SettingsIcon from '@material-ui/icons/Settings';
import ShoppingCart from "@material-ui/icons/ShoppingCart";

const styles = {
  root: {
    color: "#fff"
  },
  active: {
    color: "#000"
  }
};
const items = [
  {
    name: null,
    icon: null,
    type: "divider",
    permissions: ["IS_ADMIN", "IS_CLIENT_ADMIN"]
  },

  {//
    name: "users",
    icon: <PersonIcon />,
    permissions: ["IS_ADMIN", "IS_CLIENT_ADMIN"]
  },
  {
    name: "userRoles",
    icon: <Group />,
    permissions: ["IS_ADMIN", "IS_CLIENT_ADMIN"]
  },
  {
    name: null,
    icon: null,
    type: "divider",
    permissions: ["IS_ADMIN", "IS_CLIENT_ADMIN", "DRIVER_LIST"]
  },
  {
    name: "appUsers",
    icon: <PersonOutlineIcon />,
    permissions: ["IS_ADMIN", "IS_CLIENT_ADMIN"]
  },
  {
    name: "orders",
    icon: <ShoppingCart/>,
    permissions: ["IS_ADMIN", "IS_CLIENT_ADMIN"]
  },
];

const profile = "/profile/" + localStorage.getItem("userId");

const Menu = ({ onMenuClick, translate, logout, classes }) => {
  return (
    <WithPermissions
      render={({ permissions }) => (
        <div>
          <MenuElem
            items={items}
            onMenuClick={onMenuClick}
            translate={translate}
            logout={logout}
            profile={profile}
          />
          <Responsive
            xsmall={
              <MenuItemLink
                to={profile}
                primaryText={translate("pos.profile")}
                leftIcon={<SettingsIcon />}
                onClick={onMenuClick}
              />
            }
            medium={null}
          />
          <Responsive xsmall={logout} medium={null} />
        </div>
      )}
    />
  );
};

const enhance = compose(
  withRouter,
  connect(
    state => ({
      theme: state.theme,
      locale: state.i18n.locale
    }),
    {}
  ),
  translate,
  withStyles(styles)
);

export default enhance(Menu);
