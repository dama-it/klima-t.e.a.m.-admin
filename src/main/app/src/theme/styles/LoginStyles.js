export default theme  => ({
  main: {
    display: "flex",
    flexDirection: "column",
    minHeight: "100vh",
    alignItems: "center",
    justifyContent: "flex-start",
    background: "white",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  },
  card: {
    minWidth: 300,
    marginTop: "6em",
    boxShadow: "none",
    borderRadius: "0",
    boxShadow:"2px 2px 20px grey",
  },
  logo: {
    margin: "1em",
    display: "flex",
    justifyContent: "center"
  },
  avatar: {
    margin: "1em",
    display: "flex",
    justifyContent: "center"
  },
  icon: {
    backgroundColor: theme.palette.secondary.main
  },
  hint: {
    marginTop: "1em",
    display: "flex",
    justifyContent: "center",
    color: theme.palette.grey[500]
  },
  form: {
    padding: "0 0 1em 0"
  },
  input: {
    marginTop: "1em"
  },
  label: {
    color: "#000"
  },
  actions: {
    padding: "0 1em 1em 1em"
  },
  button: {
    backgroundColor: "#000",
    "&:hover": {
      background: "#000 !important"
    },
    color:'#fff'
  }
});
