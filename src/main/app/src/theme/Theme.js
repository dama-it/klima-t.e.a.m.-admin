export default {
  palette: {
    primary: {
      light: "#fff",
      main: "#27AAE1",
      dark: "#27AAE1",
      contrastText: "#fff"
    },
    secondary: {
      light: "#27AAE1",
      main: "#27AAE1",
      dark: "#27AAE1",
      contrastText: "#fff"
    }
  }
};
