import { GET_LIST, CREATE, GET_ONE, UPDATE } from "react-admin";
import restClient from "../providers/dataProvider/rest";


export const createProject = data => {
  return new Promise((resolve, reject) => {
    restClient(CREATE, "projects", { data: data })
      .then(data => {
        resolve(data.data);
      })
      .catch(e => {
        reject(e);
      });
  });
};

export const updateProject = (id,data) => {
  return new Promise((resolve, reject) => {
    restClient(UPDATE, "projects", { id:id, data: data })
      .then(data => {
        resolve(data.data);
      })
      .catch(e => {
        reject(e);
      });
  });
};