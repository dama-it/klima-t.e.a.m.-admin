import { SET_IDENTITY } from './actions'

export default (previousState = 1, { type, payload }) => {
  if (type === SET_IDENTITY) {
    return payload
  }
  return previousState
}
