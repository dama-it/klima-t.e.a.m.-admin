export const SET_IDENTITY = "SET_IDENTITY";

export const changeUser = userData => ({
  type: SET_IDENTITY,
  payload: userData
});
