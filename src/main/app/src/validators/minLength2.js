export const minLength2 = (
  message = "FIELD_MUST_HAVE_AT_LEAST_2_CHARACTERS"
) => (value, allValues, props) =>
  value.length > 1 || value === "" || typeof value == "undefined"
    ? undefined
    : props.translate(message);
