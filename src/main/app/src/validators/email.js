export const email = (message = "EMAIL_FORMAT") => (value, allValues, props) =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? props.translate(message)
    : undefined;
